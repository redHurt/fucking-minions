﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ScanningText : MonoBehaviour
{
    Text scannigText;
    public float scanningTime = 2f;
    public GameObject nextScreen;
    private void Awake()
    {
        scannigText = GetComponent<Text>();
    }

    public void StartScanning()
    {
        StartCoroutine(Scanning());
    }

    IEnumerator Scanning()
    {
        float t = 0f;
        while (t < 1f)
        {
            scannigText.text = $"Сканирование {(int)Mathf.Lerp(0, 100, t)}%";
            t += Time.deltaTime / scanningTime;
            yield return null;
        }

        ScreenChanger.instance.ChangeScreen(nextScreen);
    }
}
