﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MinionChanger : MonoBehaviour
{
    public Sprite[] minions;
    public int current;
    public Image holder;
    public float width = 600;

    public void Change(int dir)
    {
        int newSpriteId = current + dir;
        if (newSpriteId == minions.Length) newSpriteId = 0;
        else if (newSpriteId == -1) newSpriteId = minions.Length - 1;
        ChangeSprite(minions[newSpriteId]);
        current = newSpriteId;
    }

    public void Select(int id)
    {
        if (id >= minions.Length || id < 0)
        {
            Debug.Log("wtf, index outside bounce of array");
            return;
        }
        ChangeSprite(minions[id]);
        current = id;
    }

    void ChangeSprite(Sprite sprite)
    {
        holder.sprite = sprite;
        holder.rectTransform.sizeDelta = sprite.rect.size * width / sprite.rect.width;
    }
}