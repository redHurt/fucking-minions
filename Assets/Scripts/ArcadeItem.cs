﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArcadeItem : MonoBehaviour
{
    bool isAlive = true;
    public ItemType type;
    public ArcadeScore score;

    private void Start()
    {
        Destroy(gameObject, 10f);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Minion"))
        {
            switch (type)
            {
                case ItemType.Banana:
                    score.Score++;
                    break;
                case ItemType.Box:
                    break;
                default:
                    break;
            }
            Destroy(gameObject, .2f);
        }
    }
    //private void OnCollisionEnter2D(Collision2D collision)
    //{
    //    if (collision.gameObject.CompareTag("Minion"))
    //    {
    //        switch (type)
    //        {
    //            case ItemType.Banana:
    //                score.Score++;
    //                break;
    //            case ItemType.Box:
    //                break;
    //            default:
    //                break;
    //        }
    //        Destroy(gameObject);
    //    }
    //}
}

public enum ItemType
{
    Banana, Box
}