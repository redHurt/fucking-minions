﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenChanger : MonoBehaviour
{
    public static ScreenChanger instance;
    private void Awake()
    {
        instance = this;
    }

    /// <summary>
    /// Меняет текущую открытую страницу на выбранную
    /// </summary>
    /// <param name="nextScreen"></param>
    public void ChangeScreen(GameObject nextScreen)
    {
        GameObject currentScreen = FindCurrentScreen();

        if (currentScreen != null)
        {
            currentScreen.SetActive(false);
            nextScreen.SetActive(true);
        }
    }

    /// <summary>
    /// Поиск в root объекте текущей вкладки
    /// </summary>
    /// <returns></returns>
    GameObject FindCurrentScreen()
    {
        for (int i = 0; i < transform.childCount; i++)
        {
            if (transform.GetChild(i).gameObject.activeSelf)
                return transform.GetChild(i).gameObject;
        }
        return null;
    }
}
