﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArcadeScore : MonoBehaviour
{
    Text scoreText;

    int score;
    public int Score
    {
        get => score;
        set
        {
            score = value;
            scoreText.text = score.ToString();
        }
    }


    private void Awake()
    {
        scoreText = GetComponent<Text>();
    }
}
