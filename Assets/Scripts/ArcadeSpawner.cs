﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ArcadeSpawner : MonoBehaviour
{
    public GameObject[] prefabs;
    public bool isSpawning = false;
    public float spawnTime = 1f;
    RectTransform rect;
    public float xMaxPosition;
    public ArcadeScore score;
    private void Awake()
    {
        rect = GetComponent<RectTransform>();
    }

    public void StartSpawning()
    {
        StartCoroutine(Spawning());
    }

    public void StopSpawning()
    {
        isSpawning = false;
    }

    IEnumerator Spawning()
    {
        isSpawning = true;
        while(isSpawning)
        {
            float pos = Mathf.Sign(Random.value - .5f) * Random.Range(0f, xMaxPosition);

            ArcadeItem item = Instantiate(prefabs[Random.Range(0, prefabs.Length)], new Vector2(pos + rect.transform.position.x, rect.transform.position.y), Quaternion.identity, transform).GetComponent<ArcadeItem>();
            item.score = score;
            yield return new WaitForSeconds(spawnTime);
        }
    }
}
