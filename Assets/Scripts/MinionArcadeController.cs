﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MinionArcadeController : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    public float speed;
    bool pointerDown = false;
    public Vector3 dir;
    public Transform minion;
    public bool isrb = false;
    Rigidbody2D rb;
    private void Awake()
    {
        rb = minion.GetComponent<Rigidbody2D>();
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        pointerDown = true;
        StartCoroutine(Move());
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        pointerDown = false;
    }

    IEnumerator Move()
    {
        while(pointerDown)
        {
            if (isrb)
            {
                rb.AddForce(speed * dir);
            }
            else
                minion.Translate(speed * dir);
            yield return null;
        }
        if (isrb)
            rb.velocity = Vector2.zero;
    }
}
