﻿using UnityEngine;
using Onirix.Core.Util;

namespace Onirix.SDK.Gestures
{
    public class SwipeGestureRecognizer : MonoBehaviour
    {
        public class Event
        {
            public enum Id
            {
                Unkwnown = 0,
                SwipeBegin = 1,
                SwipeEnd = 2
            }

            public Event(Id eventId, float elapsed)
            {
                EventId = eventId;
                Elapsed = elapsed;
            }

            public Id       EventId { get; private set; }
            public float    Elapsed { get; private set; }
        }


        private const float                 MaxAngleDegrees = 5;

        public uint                         FingersRequired { get; set; } = 1;
        public EventSubscriptor<Vector2>    OnSwipe         { get; set; } = new EventSubscriptor<Vector2>();
        public EventSubscriptor<Event>      OnSwipeEvent    { get; set; } = new EventSubscriptor<Event>();

#if UNITY_EDITOR
        private Vector3                     _lastMousePos;
#endif

        private bool                        _swiping = false;
        private float                       _elapsed = 0;

        private void Update()
        {
#if UNITY_EDITOR
            // Simulate fingers used by keeping pressed the corresponding alpha number key
            if(Input.GetMouseButtonDown(0) && Input.GetKey(FingersRequired.ToString()))
            {
                _lastMousePos = Input.mousePosition;
            }
            else if (Input.GetMouseButton(0) && Input.GetKey(FingersRequired.ToString()))
            {
                Vector3 delta = Input.mousePosition - _lastMousePos;
                _lastMousePos = Input.mousePosition;

                if (delta.sqrMagnitude > 1)
                {
                    if (!_swiping)
                    {
                        _elapsed = 0;
                        _swiping = true;
                        OnSwipeEvent.Invoke(new Event(Event.Id.SwipeBegin, 0));
                    }
                    else
                    {
                        _elapsed += Time.deltaTime;
                    }

                    OnSwipe.Invoke(new Vector2(delta.x, delta.y));
                }
            }
            else if(Input.GetMouseButtonUp(0) && _swiping)
            {
                _swiping = false;
                OnSwipeEvent.Invoke(new Event(Event.Id.SwipeEnd, _elapsed));
            }
#else

            if(FingersRequired>0 && Input.touchCount == FingersRequired)
            {
                Vector2[] dirs = new Vector2[FingersRequired];
                Touch[] touches = Input.touches;
                for(int i=0; i<Input.touchCount; ++i)
                {
                    if (touches[i].phase == TouchPhase.Moved)
                    {
                        dirs[i] = touches[i].deltaPosition;
                    }
                    else
                    {
                        return;
                    }
                }

                float currentMaxAngle = 0;
                float currentAngle = 0;
                Vector2 swipeFingerMin = touches[0].deltaPosition;
                Vector2 swipeFingerMax = touches[0].deltaPosition;

                for(int i=1; i< dirs.Length; ++i)
                {
                    for (int j = 0; j < i; j++)
                    {
                        currentAngle = Vector2.Angle(dirs[i], dirs[j]);
                        if(currentAngle > MaxAngleDegrees)
                        {
                            // Angle between fingers is too big
                            return;
                        }
                        else if (currentAngle > currentMaxAngle)
                        {
                            currentMaxAngle = currentAngle;
                            swipeFingerMin = dirs[i];
                            swipeFingerMax = dirs[j];
                        }
                    }
                }

                if (!_swiping)
                {
                    _elapsed = 0;
                    _swiping = true;
                    OnSwipeEvent.Invoke(new Event(Event.Id.SwipeBegin, 0));
                }
                else
                {
                    _elapsed += Time.deltaTime;
                }

                OnSwipe.Invoke((swipeFingerMin + swipeFingerMax).normalized * Mathf.Min(swipeFingerMin.magnitude, swipeFingerMax.magnitude));
            }
            else if(_swiping && FingersRequired > 0 && Input.touchCount < FingersRequired)
            {
                _swiping = false;
                OnSwipeEvent.Invoke(new Event(Event.Id.SwipeEnd, _elapsed));
            }
#endif
        }
    }
}