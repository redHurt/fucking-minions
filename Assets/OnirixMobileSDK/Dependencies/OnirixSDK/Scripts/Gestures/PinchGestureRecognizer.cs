﻿using Onirix.Core.Util;
using UnityEngine;

namespace Onirix.SDK.Gestures
{
    public class PinchGestureRecognizer : MonoBehaviour
    {
        private const float ToleranceAngleDegrees = 30.0f;

        public EventSubscriptor<float> OnPinch { get; set; }

        private void Awake()
        {
            OnPinch = new EventSubscriptor<float>();
        }

        private void Update()
        {
            if(Input.touchCount == 2)
            {
                Touch[] touches = Input.touches;
                if(touches[0].phase == TouchPhase.Moved && touches[1].phase == TouchPhase.Moved)
                {
                    Vector2 realVec1 = touches[0].deltaPosition;
                    Vector2 realVec2 = touches[1].deltaPosition;

                    Vector2 org1 = touches[0].position - touches[0].deltaPosition;
                    Vector2 org2 = touches[1].position - touches[1].deltaPosition;

                    Vector2 goodVec1 = org2 - org1;
                    Vector2 goodVec2 = -goodVec1;

                    float angle1 = Vector2.Angle(realVec1, goodVec1);
                    float angle2 = Vector2.Angle(realVec2, goodVec2);

                    if 
                    (
                        (angle1 < ToleranceAngleDegrees && angle2 < ToleranceAngleDegrees) || 
                        (angle1 > (180 - ToleranceAngleDegrees) && angle2 > (180 - ToleranceAngleDegrees))
                    )
                    {
                        float prevDistanceSqr = goodVec1.sqrMagnitude;
                        float currDistanceSqr = (touches[0].position - touches[1].position).sqrMagnitude;

                        OnPinch.Invoke(Mathf.Sqrt(currDistanceSqr / prevDistanceSqr));
                    }
                }
            }
        }
    }
}