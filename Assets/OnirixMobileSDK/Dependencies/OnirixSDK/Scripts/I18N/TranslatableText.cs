﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Onirix.SDK.I18N
{
    [RequireComponent(typeof(Text))]
    public class TranslatableText : MonoBehaviour
    {
        private void Awake()
        {
            if(OnirixI18N.NullableInstance == null)
            {
                StartCoroutine(WaitForI18N());
            }
            else
            {
                InitializeTranslator();
            }
        }

        private IEnumerator WaitForI18N()
        {
            yield return new WaitUntil(() => { return OnirixI18N.NullableInstance != null; });
            InitializeTranslator();
        }

        private void InitializeTranslator()
        {
            // Set language translation

            Text label = GetComponent<Text>();
            label.text = OnirixI18N.Instance[label.text];

            // Subscribe for language changes
            OnirixI18N.Instance.OnLanguageSet += LanguageChanged;
        }

        private bool LanguageChanged(string code)
        {
            Text label = GetComponent<Text>();
            label.text = OnirixI18N.Instance[label.text];

            return false;
        }
    }
}