﻿using System.Collections.Generic;
using Onirix.Core.Util;
using Onirix.SDK.Util;
using UnityEngine;

namespace Onirix.SDK.I18N
{
    public class OnirixI18N : SingletonBehaviour<OnirixI18N>
    {
        private const string AppLanguage        = "AppLanguage";
        private const string DefaultLanguage    = "en";

        [SerializeField] private string         _baseFilePath;
        private string                          _language;
        private Dictionary<string, string>      _translations;

        public EventSubscriptor<string>         OnLanguageSet { get; set; } = new EventSubscriptor<string>();

        protected override void Awake()
        {
            base.Awake();

            bool success;

            if (!OnirixPrefs.HasKey(AppLanguage))
            {
                success = SetCurrentLanguage(GetSystemLanguageCode());
            }
            else
            {
                success = SetCurrentLanguage(OnirixPrefs.GetString(AppLanguage));
            }

            if (!success)
            {
                SetCurrentLanguage(DefaultLanguage);
            }
        }

        public string this[string key]
        {
            get
            {
                return Get(key);
            }
        }

        public void ResetDefaults()
        {
            SetCurrentLanguage(GetSystemLanguageCode());
            OnirixPrefs.DeleteKey(AppLanguage);
        }

        public string GetCurrentLanguageCode()
        {
            return _language;
        }

        public SystemLanguage GetCurrentLanguage()
        {
            return GetLanguageFromCode(_language);
        }

        public bool SetCurrentLanguage(string languageCode, bool remember = false)
        {
            if (_language != languageCode)
            {
                TextAsset asset = Resources.Load<TextAsset>(_baseFilePath + "_" + languageCode);

                if (asset != null)
                {
                    _translations = LitJson.JsonMapper.ToObject<Dictionary<string, string>>(asset.text);
                    _language = languageCode;

                    if (remember)
                    {
                        OnirixPrefs.SetString(AppLanguage, languageCode);
                    }

                    // Trigger events
                    OnLanguageSet.Invoke(_language);

                    return true;
                }

                return false;
            }

            return true;
        }

        public bool SetCurrentLanguage(SystemLanguage language, bool remember = false)
        {
            return SetCurrentLanguage(GetCodeForLanguage(language), remember);
        }

        public string Get(string key, string defaultValue)
        {
            string returnValue;

            if (!_translations.TryGetValue(key, out returnValue))
            {
                returnValue = defaultValue;
            }

            return returnValue;
        }

        public string Get(string key)
        {
            return Get(key, key);
        }

        public static string GetSystemLanguageCode()
        {
            return GetCodeForLanguage(Application.systemLanguage);
        }

        public static SystemLanguage GetLanguageFromCode(string code)
        {
            switch (code)
            {
            case "af":
                return SystemLanguage.Afrikaans;
            case "ar":
                return SystemLanguage.Arabic;
            case "eu":
                return SystemLanguage.Basque;
            case "by":
                return SystemLanguage.Belarusian;
            case "bg":
                return SystemLanguage.Bulgarian;
            case "ca":
                return SystemLanguage.Catalan;
            case "zh":
                return SystemLanguage.Chinese;
            case "cs":
                return SystemLanguage.Czech;
            case "da":
                return SystemLanguage.Danish;
            case "nl":
                return SystemLanguage.Dutch;
            case "en":
                return SystemLanguage.English;
            case "et":
                return SystemLanguage.Estonian;
            case "fo":
                return SystemLanguage.Faroese;
            case "fi":
                return SystemLanguage.Finnish;
            case "fr":
                return SystemLanguage.French;
            case "de":
                return SystemLanguage.German;
            case "el":
                return SystemLanguage.Greek;
            case "iw":
                return SystemLanguage.Hebrew;
            case "hu":
                return SystemLanguage.Hungarian;
            case "is":
                return SystemLanguage.Icelandic;
            case "in":
                return SystemLanguage.Indonesian;
            case "it":
                return SystemLanguage.Italian;
            case "ja":
                return SystemLanguage.Japanese;
            case "ko":
                return SystemLanguage.Korean;
            case "lv":
                return SystemLanguage.Latvian;
            case "lt":
                return SystemLanguage.Lithuanian;
            case "no":
                return SystemLanguage.Norwegian;
            case "pl":
                return SystemLanguage.Polish;
            case "pt":
                return SystemLanguage.Portuguese;
            case "ro":
                return SystemLanguage.Romanian;
            case "ru":
                return SystemLanguage.Russian;
            case "sh":
                return SystemLanguage.SerboCroatian;
            case "sk":
                return SystemLanguage.Slovak;
            case "sl":
                return SystemLanguage.Slovenian;
            case "es":
                return SystemLanguage.Spanish;
            case "sv":
                return SystemLanguage.Swedish;
            case "th":
                return SystemLanguage.Thai;
            case "tr":
                return SystemLanguage.Turkish;
            case "uk":
                return SystemLanguage.Ukrainian;
            case "vi":
                return SystemLanguage.Vietnamese;
            case "zh-Hans":
                return SystemLanguage.ChineseSimplified;
            case "zh-Hant":
                return SystemLanguage.ChineseTraditional;
            default:
                return SystemLanguage.Unknown;
            }
        }

        public static string GetCodeForLanguage(SystemLanguage language)
        {
            switch (language)
            {
            case SystemLanguage.Afrikaans:
                return "af";
            case SystemLanguage.Arabic:
                return "ar";
            case SystemLanguage.Basque:
                return "eu";
            case SystemLanguage.Belarusian:
                return "by";
            case SystemLanguage.Bulgarian:
                return "bg";
            case SystemLanguage.Catalan:
                return "ca";
            case SystemLanguage.Chinese:
                return "zh";
            case SystemLanguage.Czech:
                return "cs";
            case SystemLanguage.Danish:
                return "da";
            case SystemLanguage.Dutch:
                return "nl";
            case SystemLanguage.English:
                return "en";
            case SystemLanguage.Estonian:
                return "et";
            case SystemLanguage.Faroese:
                return "fo";
            case SystemLanguage.Finnish:
                return "fi";
            case SystemLanguage.French:
                return "fr";
            case SystemLanguage.German:
                return "de";
            case SystemLanguage.Greek:
                return "el";
            case SystemLanguage.Hebrew:
                return "iw";
            case SystemLanguage.Hungarian:
                return "hu";
            case SystemLanguage.Icelandic:
                return "is";
            case SystemLanguage.Indonesian:
                return "in";
            case SystemLanguage.Italian:
                return "it";
            case SystemLanguage.Japanese:
                return "ja";
            case SystemLanguage.Korean:
                return "ko";
            case SystemLanguage.Latvian:
                return "lv";
            case SystemLanguage.Lithuanian:
                return "lt";
            case SystemLanguage.Norwegian:
                return "no";
            case SystemLanguage.Polish:
                return "pl";
            case SystemLanguage.Portuguese:
                return "pt";
            case SystemLanguage.Romanian:
                return "ro";
            case SystemLanguage.Russian:
                return "ru";
            case SystemLanguage.SerboCroatian:
                return "sh";
            case SystemLanguage.Slovak:
                return "sk";
            case SystemLanguage.Slovenian:
                return "sl";
            case SystemLanguage.Spanish:
                return "es";
            case SystemLanguage.Swedish:
                return "sv";
            case SystemLanguage.Thai:
                return "th";
            case SystemLanguage.Turkish:
                return "tr";
            case SystemLanguage.Ukrainian:
                return "uk";
            case SystemLanguage.Vietnamese:
                return "vi";
            case SystemLanguage.ChineseSimplified:
                return "zh-Hans";
            case SystemLanguage.ChineseTraditional:
                return "zh-Hant";
            case SystemLanguage.Unknown:
                return "";
            }

            return "";
        }
    }
}
