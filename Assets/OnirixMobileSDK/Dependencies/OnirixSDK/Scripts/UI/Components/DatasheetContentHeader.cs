﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Onirix.SDK.UI.Components
{
    public class DatasheetContentHeader : MonoBehaviour
    {
        private const float LinePadding = 55;

        [SerializeField] RectTransform  _object3dIcon;
        [SerializeField] RectTransform  _object2dIcon;
        [SerializeField] Text           _labelIconText;
        [SerializeField] Image          _labelIconBackground;
        private Mode                    _mode;

        public enum Mode
        {
            Element3D,
            Element2D,
            Label
        }

        public Mode HeaderMode
        {
            get
            {
                return _mode;
            }
            set
            {
                if(_mode != value)
                {
                    _mode = value;

                    if(_mode == Mode.Label)
                    {
                        _object2dIcon.gameObject.SetActive(false);
                        _object3dIcon.gameObject.SetActive(false);
                        _labelIconBackground.gameObject.SetActive(true);

                    }
                    else if (_mode == Mode.Element2D)
                    {
                        _object2dIcon.gameObject.SetActive(true);
                        _object3dIcon.gameObject.SetActive(false);
                        _labelIconBackground.gameObject.SetActive(false);
                    }
                    else
                    {
                        _object2dIcon.gameObject.SetActive(false);
                        _object3dIcon.gameObject.SetActive(true);
                        _labelIconBackground.gameObject.SetActive(false);
                    }
                }
            }
        }

        public Color LabelForeground
        {
            get
            {
                return _labelIconText.color;
            }
            set
            {
                _labelIconText.color = value;
            }
        }

        public Color LabelBackground
        {
            get
            {
                return _labelIconBackground.color;
            }
            set
            {
                _labelIconBackground.color = value;
            }
        }

        public string LabelText
        {
            get
            {
                return _labelIconText.text;
            }
            set
            {
                _labelIconText.text = value;
            }
        }

        // Use this for initialization
        void Start()
        {
            _mode = Mode.Element3D;
        }
    }
}