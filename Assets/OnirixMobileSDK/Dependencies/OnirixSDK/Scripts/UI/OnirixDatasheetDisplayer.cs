﻿using System;
using Onirix.Core;
using Onirix.Core.Util;
using Onirix.Core.Model;
using Onirix.Core.Service;
using Onirix.SDK.UI.ViewControllers;
using UnityEngine;

using Action = Onirix.Core.Model.Action;


namespace Onirix.SDK.UI
{
    public class OnirixDatasheetDisplayer : SingletonBehaviour<OnirixDatasheetDisplayer>
    {
        [SerializeField] int    _datasheetViewIndex;
        [SerializeField] string _navigationControllerName = OnirixUIManager.DefaultNavigationControllerName;

        void Start()
        {
            OnirixActionHandler.Instance.RegisterActionCallback(ActionType.ShowDatasheet, ShowDatasheet);
        }

        private void ShowDatasheet(Action action, GameObject target, GameObject element)
        {
            GameObject dsView = OnirixUIManager.Instance.CreateView(_datasheetViewIndex);
            DatasheetViewController vc = dsView.GetComponent<DatasheetViewController>();

            if (vc)
            {
                vc.Init(OnirixSceneLoader.GetCloneOid(target), element, action.Datasheet);
                OnirixUIManager.Instance[_navigationControllerName].PushView(dsView);
            }
            else
            {
                throw new ArgumentException(string.Format("OnirixDatasheetDisplayer: view with index {0} in " +
                    "OnirixUIManager doesn't contain an UIViewController deriving from DatasheetViewController", _datasheetViewIndex));
            }
        }
    }
}