﻿using System;
using System.Collections.Generic;
using Onirix.Core.Util;
using UnityEngine;
using UnityEngine.UI;

namespace Onirix.SDK.UI
{
    public class OnirixUIManager : SingletonBehaviour<OnirixUIManager>
    {
        public const string             DefaultNavigationControllerName = "default";
        public const string             DefaultNavigationControllerType = "Onirix.SDK.UI.UINavigationController";
        public const short              DefaultNavigationControllerPriority = 0;
        public static readonly Vector2  DefaultReferenceResolution = new Vector2(1080, 1920);
        public const float              DefaultReferencePixelsPerUnit = 100;
        public const float              DefaultMatchWidthOrHeight = 0.5f;

        [SerializeField] private GameObject[]               _views;

        [Header("Default Navigation Controller Settings")]
        [SerializeField] private string                     _navigationControllerName = DefaultNavigationControllerName;
        [SerializeField] private string                     _navigationControllerType = DefaultNavigationControllerType;
        [SerializeField] private Vector2                    _referenceResolution = DefaultReferenceResolution;
        [SerializeField] private float                      _referencePixelsPerUnit = DefaultReferencePixelsPerUnit;
        [SerializeField][Range(0,1)] private float          _matchWidthOrHeight = DefaultMatchWidthOrHeight;
        [SerializeField] private short                      _navigationControllerPriority = DefaultNavigationControllerPriority;

        private Dictionary<string, UINavigationController>  _navigationControllers = new Dictionary<string, UINavigationController>();
        private Dictionary<string, Canvas>                  _canvases  = new Dictionary<string, Canvas>();

        public UINavigationController DefaultNavigationController
        {
            get
            {
                return _navigationControllers[DefaultNavigationControllerName];
            }
        }

        public UINavigationController this[string name]
        {
            get
            {
                return _navigationControllers[name];
            }
        }

        private void Update()
        {
            if (Input.GetKeyDown(KeyCode.Escape))
            {
                Canvas activeCanvas = null;
                foreach (var pair in _canvases)
                {
                    if(pair.Value.transform.childCount > 0)
                    {
                        if(activeCanvas==null || activeCanvas.sortingOrder<pair.Value.sortingOrder)
                        {
                            activeCanvas = pair.Value;
                        }
                    }
                }

                if(activeCanvas != null)
                {
                    _navigationControllers[activeCanvas.name].OnBack();
                }
            }
        }

        protected override void Awake()
        {
            base.Awake();

            Type t = Type.GetType(_navigationControllerType);

            // Create default UINavigationController
            CreateNavigationController
            (
                Type.GetType(_navigationControllerType),
                _navigationControllerName,
                _navigationControllerPriority,
                _referenceResolution,
                _referencePixelsPerUnit,
                _matchWidthOrHeight
            );
        }

        public UINavigationController CreateNavigationController(string name, short priority, Vector2 resolution, float pixelsPerUnit, float matchWidthOrHeight)
        {
            return CreateNavigationController<UINavigationController>(name, priority, resolution, pixelsPerUnit, matchWidthOrHeight);
        }

        public T CreateNavigationController<T>(string name, short priority, Vector2 resolution, float pixelsPerUnit, float matchWidthOrHeight) where T : UINavigationController
        {
            GameObject canvasGO = CreateCanvas(name, priority, resolution, pixelsPerUnit, matchWidthOrHeight);

            T nc = canvasGO.AddComponent<T>();
            _navigationControllers[name] = nc;

            return nc;
        }

        protected UINavigationController CreateNavigationController(Type ncType, string name, short priority, Vector2 resolution, float pixelsPerUnit, float matchWidthOrHeight)
        {
            if (typeof(UINavigationController).IsAssignableFrom(ncType))
            {
                GameObject canvasGO = CreateCanvas(name, priority, resolution, pixelsPerUnit, matchWidthOrHeight);

                UINavigationController nc = canvasGO.AddComponent(ncType) as UINavigationController;
                _navigationControllers[name] = nc;

                return nc;
            }

            return null;
        }

        protected GameObject CreateCanvas(string canvasName, short priority, Vector2 resolution, float pixelsPerUnit, float matchWidthOrHeight)
        {
            GameObject canvasGO = new GameObject();
            canvasGO.name = canvasName;

            Canvas canvas = canvasGO.AddComponent<Canvas>();
            canvas.sortingOrder = priority;
            canvas.renderMode = RenderMode.ScreenSpaceOverlay;

            CanvasScaler canvasScaler = canvasGO.AddComponent<CanvasScaler>();
            canvasScaler.uiScaleMode = CanvasScaler.ScaleMode.ScaleWithScreenSize;
            canvasScaler.screenMatchMode = CanvasScaler.ScreenMatchMode.MatchWidthOrHeight;
            canvasScaler.referenceResolution = resolution;
            canvasScaler.matchWidthOrHeight = matchWidthOrHeight;
            canvasScaler.referencePixelsPerUnit = pixelsPerUnit;

            canvasGO.AddComponent<OnirixGraphicRaycaster>();

            _canvases[canvasName] = canvas;

            return canvasGO;
        }

        public bool NavigationControllerExists(string name)
        {
            return _navigationControllers.ContainsKey(name);
        }

        public void DestroyNavigationController(string name)
        {
            Canvas canvas = null;
            if(_canvases.TryGetValue(name, out canvas))
            {
                _canvases.Remove(name);
                _navigationControllers.Remove(name);
                Destroy(canvas.gameObject);
            }
        }

        public GameObject CreateView(int index)
        {
            if (index < _views.Length)
            {
                GameObject view = Instantiate(_views[index]);
                CanvasGroup cg = view.GetComponent<CanvasGroup>();

                // Automatically add a CanvasGroup component to the view if not present
                if(cg == null)
                {
                    view.AddComponent<CanvasGroup>();
                }

                return view;
            }

            Debug.Log("Onirix.SDK.UI.UIManager: Trying to create view with invalid index");
            return null;
        }
    }
} // end namespace Onirix.SDK.UI
