﻿using UnityEngine;

namespace Onirix.SDK.UI
{ 
    [RequireComponent(typeof(CanvasGroup))]
    public class UIViewController : MonoBehaviour
    {
        private bool _focused = false;

        public UINavigationController NavigationController { get; set; }

        public bool Focused
        {
            get
            {
                return _focused;
            }
        }

        public GameObject View
        {
            get
            {
                return gameObject;
            }
        }

        public virtual void OnEntersBackground()
        {
            // Keeps on the hierarchy... maybe make half transparent? SetActive(false)
            CanvasGroup cg = View.GetComponent<CanvasGroup>();
            if (cg != null)
            {
                View.GetComponent<CanvasGroup>().interactable = false;
            }
            else
            {
                Debug.Log("ViewController: No CanvasGroup component found!");
            }

            _focused = false;
        }

        public virtual void OnEntersForeground(bool newView)
        {
            // Add to hierarchy
            if (newView)
            {
                View.transform.SetParent(NavigationController.gameObject.transform, false);
            }

            CanvasGroup cg = View.GetComponent<CanvasGroup>();
            if (cg != null)
            {
                View.GetComponent<CanvasGroup>().interactable = true;
            }
            else
            {
                Debug.Log("ViewController: No CanvasGroup component found!");
            }

            View.SetActive(true);
            _focused = true;
        }

        public virtual void OnPopped()
        {
            // Remove from hierarchy
            View.SetActive(false);

            // Destroy view
            Destroy(View);
            _focused = false;
        }

        public virtual void OnBack()
        {
            if (NavigationController.GetTopViewController() == this)
            {
                NavigationController.PopViewController();
            }
        }
    }

} // end namespace Onirix.SDK.UI
