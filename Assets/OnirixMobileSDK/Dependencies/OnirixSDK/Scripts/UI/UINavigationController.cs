﻿using System.Collections.Generic;
using UnityEngine;

namespace Onirix.SDK.UI
{
    public class UINavigationController : MonoBehaviour
    {        
        protected Stack<UIViewController> _viewControllers = new Stack<UIViewController>();

        public virtual void OnBack()
        {
            if (_viewControllers.Count>0)
            {
                GetTopViewController().OnBack();
            }
        }

        public void OnDestroy()
        {
            foreach(UIViewController c in _viewControllers)
            {
                if(typeof(INavigationControllerEvents).IsAssignableFrom(c.GetType()))
                {
                    (c as INavigationControllerEvents).OnNavigationControllerDestroy();
                }
            }

            while (PopViewController() != null)
            {
            }
        }

        public void PushView(GameObject view)
        {
            if(view != null)
            {
                PushViewController(view.GetComponent<UIViewController>());
            }
        }

        public virtual void PushViewController(UIViewController vc)
        {
            if(vc != null)
            {
                if(_viewControllers.Count>0)
                {
                    // Notify old view controller about entering background
                    GetTopViewController().OnEntersBackground();
                }

                _viewControllers.Push(vc);
                vc.NavigationController = this;

                // Notify new view controller about entering foreground
                vc.OnEntersForeground(true);
            }       
            else
            {
                Debug.LogError("Unable to push <null> UIViewController");
            }
        }

        public virtual UIViewController PopViewController()
        {
            UIViewController vc = null;

            if(_viewControllers.Count>0)
            {
                // Notify top view controller about being popped
                GetTopViewController().OnPopped();
                vc = _viewControllers.Pop();
                vc.NavigationController = null;

                // Notify new view controller about entering foreground
                if(_viewControllers.Count>0)
                {
                    GetTopViewController().OnEntersForeground(false);
                }
            }

            return vc;
        }

        public UIViewController GetTopViewController()
        {
            return _viewControllers.Peek();
        }

        public Stack<UIViewController> GetViewControllers()
        {
            return _viewControllers;
        }
        
    }

} // end namespace Onirix.SDK.UI
