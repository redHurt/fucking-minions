﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace Onirix.SDK.UI
{
    public class OnirixGraphicRaycaster : GraphicRaycaster
    {
       public static readonly string[] DefaultBlockingMask = { "UI" };
       
        protected override void Awake()
        {
            base.Awake();
            m_BlockingMask = LayerMask.GetMask(DefaultBlockingMask);
        }
    }
}
