﻿using UnityEngine;

namespace Onirix.SDK.UI
{
    public interface INavigationControllerEvents
    {
        void OnNavigationControllerDestroy();
    }

} // end namespace Onirix.SDK.UI
