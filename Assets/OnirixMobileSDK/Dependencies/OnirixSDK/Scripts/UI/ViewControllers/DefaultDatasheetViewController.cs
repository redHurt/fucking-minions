﻿using System;
using UnityEngine;
using UnityEngine.UI;
using Onirix.Core.Model;
using Onirix.SDK.Util;
using Onirix.SDK.UI.Components;
using Onirix.Core.UI;

namespace Onirix.SDK.UI.ViewControllers
{
    public class DefaultDatasheetViewController : DatasheetViewController
    {
        [SerializeField] private DatasheetContentHeader _header;
        [SerializeField] private RectTransform          _frameParent;
        [SerializeField] private GameObject             _topPadding;
        [SerializeField] private Button                 _closeButton;
        [SerializeField] private Button                 _urlButton;
        [SerializeField] private Text                   _urlText;
        [SerializeField] private Text                   _urlUnderlineText;
        [SerializeField] private Text                   _title;
        [SerializeField] private Text                   _description;
        [SerializeField] private Image                  _imageBackground;
        [SerializeField] private Image                  _image;

        private ScreenOrientation                       _orientation;

        public override void Init(string containerOid, GameObject element, Datasheet datasheet)
        {
            base.Init(containerOid, element, datasheet);

            _orientation = ScreenOrientation.Unknown;

            _closeButton.onClick.AddListener(() => OnBack());

            OnirixLabel label = element.GetComponent<OnirixLabel>();
            if(label != null)
            {
                _header.HeaderMode = DatasheetContentHeader.Mode.Label;
                _header.LabelForeground = label.ForegroundColor;
                _header.LabelBackground = label.BackgroundColor;
                _header.LabelText = label.Text;
            }
            else
            {
                StringMetaData metadata = element.GetComponent<StringMetaData>();
                if(metadata != null && metadata[0] == "element2d")
                {
                    _header.HeaderMode = DatasheetContentHeader.Mode.Element2D;
                }
                else
                {
                    _header.HeaderMode = DatasheetContentHeader.Mode.Element3D;
                }
            }
        }

        protected override void SetImage(Texture2D image)
        {
            if(image != null)
            {
                _imageBackground.gameObject.SetActive(true);
                _topPadding.SetActive(false);

                _image.sprite = Sprite.Create(image, new Rect(0, 0, image.width, image.height), Vector2.zero);
                _image.GetComponent<AspectRatioFitter>().aspectRatio = image.width/(float)image.height;
            }
        }

        protected override void SetTitle(string title)
        {
            _title.text = title;
        }

        protected override void SetDescription(string description)
        {
            _description.text = description;
        }

        protected override void SetUrl(string title, string url)
        {
            // Show URL button
            _urlButton.gameObject.SetActive(true);

            // Update URL text
            _urlText.text = title;

            // Update URL underline
            int size = TextUtil.GetTextLength(_urlText.text, _urlText.font, _urlText.fontSize, _urlText.fontStyle);
            _urlUnderlineText.text = "";
            while(TextUtil.GetTextLength(_urlUnderlineText.text, _urlUnderlineText.font, _urlUnderlineText.fontSize, _urlUnderlineText.fontStyle)<size)
            {
                _urlUnderlineText.text += "_";
            }

            // Set callback
            _urlButton.onClick.AddListener(() => Application.OpenURL(url));
        }

        private void LateUpdate()
        {
            if (_orientation != Screen.orientation)
            {
                _orientation = Screen.orientation;

                if (Screen.orientation == ScreenOrientation.LandscapeLeft || Screen.orientation == ScreenOrientation.LandscapeRight)
                {
                    UseLandscapeLayout();
                }
                else if (Screen.orientation == ScreenOrientation.Portrait || Screen.orientation == ScreenOrientation.PortraitUpsideDown)
                {
                    UsePortraitLayout();
                }
            }
        }

        private void UseLandscapeLayout()
        {
            _frameParent.offsetMin = new Vector2(350, _frameParent.offsetMin.y);
            _frameParent.offsetMax = new Vector2(-350, _frameParent.offsetMax.y);
        }

        private void UsePortraitLayout()
        {
            _frameParent.offsetMin = new Vector2(0, _frameParent.offsetMin.y);
            _frameParent.offsetMax = new Vector2(0, _frameParent.offsetMax.y);
        }
    }
}