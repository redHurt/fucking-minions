﻿using System;
using Onirix.Core.Model;
using Onirix.Core.Service;
using UnityEngine;

namespace Onirix.SDK.UI.ViewControllers
{
    public abstract class DatasheetViewController : UIViewController
    {
        public const string     DefaultTemplateName = "Default";
        protected const string  ContainerType = "0";

        private Texture2D _imageTex = null;

        public virtual void Init(string containerOid, GameObject element, Datasheet datasheet)
        {
            if (datasheet.Template.Name.Equals(DefaultTemplateName, StringComparison.OrdinalIgnoreCase))
            {
                object value = null;

                if (datasheet.Content.TryGetValue("title", out value))
                {
                    SetTitle(value as string);
                }

                if (datasheet.Content.TryGetValue("description", out value))
                {
                    SetDescription(value as string);
                }

                if (datasheet.Content.TryGetValue("image", out value))
                {
                    OnirixRestService.Instance.GetAssetData(value as string, (asset, error) =>
                    {
                        if (asset != null)
                        {
                            _imageTex = OnirixAssetLoader.Instance.DownloadImageAsTexture(asset.Filename, asset.Oid,
                                ContainerType, containerOid);

                            SetImage(_imageTex);
                        }
                    });
                }

                if (datasheet.Content.TryGetValue("url", out value))
                {
                    string url = value as string;

                    if (datasheet.Content.TryGetValue("url title", out value))
                    {
                        string title = value as string;

                        SetUrl(string.IsNullOrEmpty(title)?url:title, url);
                    }
                    else
                    {
                        SetUrl(url, url);
                    }
                }
            }
        }

        public void OnDestroy()
        {
            if(_imageTex!=null)
            {
                Destroy(_imageTex);
            }
        }

        protected abstract void SetImage(Texture2D image);
        protected abstract void SetTitle(string title);
        protected abstract void SetDescription(string description);
        protected abstract void SetUrl(string title, string url);
    }
}