﻿using UnityEngine;
using Onirix.Core.Util;

namespace Onirix.SDK.UI
{
    public class UIRevealNavigationController : UINavigationController
    {
        // Reveal status metadata
        private class RevealInfo
        {
            public enum Status
            {
                NeverOpened,
                Open,
                Closed
            }

            public Status RevealStatus { get; set; }
        }

        private class RevealInfoMetaData : MetaData<RevealInfo>
        {
        }


        protected UIViewController  _revealViewController = null;

        public bool IsRevealMenuOpen
        {
            get
            {
                return _revealViewController?.GetComponent<RevealInfoMetaData>()?.Data.RevealStatus == RevealInfo.Status.Open;
            }
        }

        public UIViewController GetRevealViewController()
        {
            return _revealViewController;
        }

        public override void OnBack()
        {
            Transform t = _revealViewController.View.transform;
            if (IsRevealMenuOpen && t.GetSiblingIndex() == t.parent.childCount-1)
            {
                _revealViewController.OnBack();
            }
            else
            {
                base.OnBack();
            }
        }

        public new void OnDestroy()
        {
            SetRevealViewController(null);
            base.OnDestroy();
        }

        public void SetRevealView(GameObject view)
        {
            SetRevealViewController(view?.GetComponent<UIViewController>());
        }

        public void SetRevealViewController(UIViewController vc)
        {
            bool wasOpen = IsRevealMenuOpen;

            if(_revealViewController != null)
            {
                _revealViewController.OnPopped();
                _revealViewController.NavigationController = null;
            }

            _revealViewController = vc;

            if (_revealViewController != null)
            {
                _revealViewController.NavigationController = this;
                _revealViewController.View.transform.SetParent(gameObject.transform, false);
                _revealViewController.View.transform.SetAsFirstSibling();
                _revealViewController.View.SetActive(false);


                // Reveal info metadata
                RevealInfoMetaData metaData = _revealViewController.View.AddComponent<RevealInfoMetaData>();
                RevealInfo info = new RevealInfo();
                info.RevealStatus = RevealInfo.Status.NeverOpened;
                metaData.Data = info;

                if (wasOpen)
                {
                    OpenRevealViewController();
                }
            }
        }

        public void OpenRevealViewController()
        {
            if (!IsRevealMenuOpen)
            {
                RevealInfoMetaData metaData = _revealViewController.GetComponent<RevealInfoMetaData>();

                bool firstEnter = (metaData.Data.RevealStatus == RevealInfo.Status.NeverOpened);

                // Move to the front
                _revealViewController.View.transform.SetAsLastSibling();

                // Notify reveal view controller about entering foreground
                _revealViewController?.OnEntersForeground(firstEnter);

                // Mark as opened
                metaData.Data.RevealStatus = RevealInfo.Status.Open;
            }
        }

        public void CloseRevealViewController()
        {
            if(IsRevealMenuOpen)
            {
                // Notify reveal view controller about entering background
                _revealViewController.OnEntersBackground();

                // Move to the back
                _revealViewController.View.transform.SetAsFirstSibling();

                // Mark as opened
                RevealInfoMetaData metaData = _revealViewController.GetComponent<RevealInfoMetaData>();
                metaData.Data.RevealStatus = RevealInfo.Status.Closed;
            }
        }
    }
}