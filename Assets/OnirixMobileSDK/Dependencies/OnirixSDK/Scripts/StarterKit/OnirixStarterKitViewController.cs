﻿using UnityEngine;
using UnityEngine.UI;
using Onirix.SDK.UI;

namespace Onirix.SDK.StarterKit
{
    public class OnirixStarterKitViewController : UIViewController
    {
        private Image _background;

        public override void OnEntersForeground(bool newView)
        {
            base.OnEntersForeground(newView);

            if(newView)
            {
                _background = GetComponent<Image>();

                GetComponent<Button>().onClick.AddListener
                (
                    () =>
                    {
                        _background.color = Random.ColorHSV();
                    }
                );
            }
        }

        public override void OnBack()
        {
            // Disable window pop
        }
    }
}
