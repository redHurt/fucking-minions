﻿using Onirix.SDK.UI;
using UnityEngine;

namespace Onirix.SDK.StarterKit
{
    public class OnirixDefaultController : MonoBehaviour
    {
        void Start()
        {
            // Configure screen to never sleep
            Screen.sleepTimeout = SleepTimeout.NeverSleep;

            // Push view on index 0
            OnirixUIManager.Instance.DefaultNavigationController.PushView(OnirixUIManager.Instance.CreateView(0));
        }
    }
}
