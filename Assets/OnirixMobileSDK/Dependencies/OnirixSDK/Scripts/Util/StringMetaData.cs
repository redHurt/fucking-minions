﻿using UnityEngine;

namespace Onirix.SDK.Util
{
    public class StringMetaData : MonoBehaviour
    {
        [SerializeField] private string[] _metadata;

        public string this[int index]
        {
            get
            {
                return _metadata[index];
            }
        }
    }
}