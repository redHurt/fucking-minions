﻿using System;

namespace Onirix.SDK.Util
{
    public class ObjRef<T>
    {
        public T Value { get; set; }
    }
}
