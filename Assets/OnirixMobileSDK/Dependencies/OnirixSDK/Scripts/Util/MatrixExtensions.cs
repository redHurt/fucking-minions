﻿using UnityEngine;

namespace Onirix.SDK.Util
{
    public static class MatrixExtensions
    {
        public static Quaternion ExtractRotation(this Matrix4x4 matrix)
        {
            Vector3 forward;
            forward.x = matrix.m02;
            forward.y = matrix.m12;
            forward.z = matrix.m22;

            Vector3 upwards;
            upwards.x = matrix.m01;
            upwards.y = matrix.m11;
            upwards.z = matrix.m21;

            return Quaternion.LookRotation(forward, upwards);
        }

        public static Vector3 ExtractPosition(this Matrix4x4 matrix)
        {
            Vector3 position;
            position.x = matrix.m03;
            position.y = matrix.m13;
            position.z = matrix.m23;
            return position;
        }

        public static Vector3 ExtractScale(this Matrix4x4 matrix)
        {
            Vector3 scale;
            scale.x = new Vector4(matrix.m00, matrix.m10, matrix.m20, matrix.m30).magnitude;
            scale.y = new Vector4(matrix.m01, matrix.m11, matrix.m21, matrix.m31).magnitude;
            scale.z = new Vector4(matrix.m02, matrix.m12, matrix.m22, matrix.m32).magnitude;

            return scale;
        }

        public static Matrix4x4 FromOpenCVPose(double[] pose)
        {
            Matrix4x4 tmp = new Matrix4x4();

            tmp.m00 = (float)pose[0];
            tmp.m01 = (float)pose[1];
            tmp.m02 = (float)pose[2];
            tmp.m03 = (float)pose[3]; // 0
            tmp.m10 = (float)pose[4];
            tmp.m11 = (float)pose[5];
            tmp.m12 = (float)pose[6];
            tmp.m13 = (float)pose[7]; // 0
            tmp.m20 = (float)pose[8];
            tmp.m21 = (float)pose[9];
            tmp.m22 = (float)pose[10];
            tmp.m23 = (float)pose[11]; // 0
            tmp.m30 = (float)pose[12];
            tmp.m31 = (float)pose[13];
            tmp.m32 = (float)pose[14];
            tmp.m33 = (float)pose[15]; // 1

            // The given openCVPose is actually a OpenGL transformation matrix, where the translation
            // elements are in the m03, m13 and m23, so we have to transpose the matrix.

            tmp = tmp.transpose;
            
            Vector3 normal = new Vector3(tmp.m02, tmp.m12, tmp.m22);
            Vector3 upwards = new Vector3(tmp.m01, tmp.m11, tmp.m21) * -1;
            Vector3 translation = new Vector3(tmp.m03, tmp.m13, -tmp.m23);
            Quaternion rotation = Quaternion.LookRotation(normal.normalized, upwards.normalized).normalized;

            // TODO: Maybe we should extract the actual matrix scale...
            return Matrix4x4.TRS(translation, rotation, Vector3.one);
        }
    }
}
