﻿using System.Globalization;
using System.Text;
using UnityEngine;

namespace Onirix.SDK.Util
{
    public static class TextUtil
    {
        public static int GetTextLength(string message, Font font, int fontSize, FontStyle style)
        {
            int totalLength = 0;

            CharacterInfo characterInfo = new CharacterInfo();

            font.RequestCharactersInTexture(message, fontSize, style);

            char[] arr = message.ToCharArray();

            foreach (char c in arr)
            {
                font.GetCharacterInfo(c, out characterInfo, fontSize, style);
                totalLength += characterInfo.advance;
            }

            return totalLength;
        }
    }
}
