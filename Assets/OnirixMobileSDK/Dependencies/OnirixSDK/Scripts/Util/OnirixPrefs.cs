﻿using UnityEngine;

namespace Onirix.SDK.Util
{
    public static class OnirixPrefs
    {
        private const string ArrayEntryFormat = "{0}_{{{1}}}";
        private const string ArrayLengthProperty = "Count";

        public static void SetBool(string key, bool value)
        {
            PlayerPrefs.SetInt(key, value ? 1 : 0);
        }

        public static bool GetBool(string key, bool defaultValue)
        {
            return PlayerPrefs.GetInt(key, defaultValue ? 1 : 0) == 1;
        }

        public static bool GetBool(string key)
        {
            return PlayerPrefs.GetInt(key) == 1;
        }

        public static void SetInt(string key, int value)
        {
            PlayerPrefs.SetInt(key, value);
        }

        public static int GetInt(string key, int defaultValue)
        {
            return PlayerPrefs.GetInt(key, defaultValue);
        }

        public static int GetInt(string key)
        {
            return PlayerPrefs.GetInt(key);
        }

        public static void SetFloat(string key, float value)
        {
            PlayerPrefs.SetFloat(key, value);
        }

        public static float GetFloat(string key, float defaultValue)
        {
            return PlayerPrefs.GetFloat(key, defaultValue);
        }

        public static float GetFloat(string key)
        {
            return PlayerPrefs.GetFloat(key);
        }

        public static void SetString(string key, string value)
        {
            PlayerPrefs.SetString(key, value);
        }

        public static string GetString(string key, string defaultValue)
        {
            return PlayerPrefs.GetString(key, defaultValue);
        }

        public static string GetString(string key)
        {
            return PlayerPrefs.GetString(key);
        }

        public static bool HasKey(string key)
        {
            if(PlayerPrefs.HasKey(key) || HasArray(key))
            {
                return true;
            }

            return false;
        }

        public static bool[] GetBoolArray(string key)
        {
            bool[] result = null;

            string countEntry = string.Format(ArrayEntryFormat, key, ArrayLengthProperty);
            if (PlayerPrefs.HasKey(countEntry))
            {
                int size = PlayerPrefs.GetInt(countEntry);

                result = new bool[size];

                for (int i = 0; i < size; ++i)
                {
                    string keyEntry = string.Format(ArrayEntryFormat, key, i);
                    if (PlayerPrefs.HasKey(keyEntry))
                    {
                        result[i] = GetBool(keyEntry);
                    }
                    else
                    {
                        result = null;
                        break;
                    }
                }
            }

            return result;
        }

        public static void SetBoolArray(string key, bool[] value)
        {
            string countEntry = string.Format(ArrayEntryFormat, key, ArrayLengthProperty);
            PlayerPrefs.SetInt(countEntry, value.Length);

            for (int i = 0; i < value.Length; ++i)
            {
                SetBool(string.Format(ArrayEntryFormat, key, i), value[i]);
            }
        }


        public static int[] GetIntArray(string key)
        {
            int[] result = null;

            string countEntry = string.Format(ArrayEntryFormat, key, ArrayLengthProperty);
            if (PlayerPrefs.HasKey(countEntry))
            {
                int size = PlayerPrefs.GetInt(countEntry);

                result = new int[size];

                for (int i = 0; i < size; ++i)
                {
                    string keyEntry = string.Format(ArrayEntryFormat, key, i);
                    if (PlayerPrefs.HasKey(keyEntry))
                    {
                        result[i] = PlayerPrefs.GetInt(keyEntry);
                    }
                    else
                    {
                        result = null;
                        break;
                    }
                }
            }

            return result;
        }

        public static void SetIntArray(string key, int[] value)
        {
            string countEntry = string.Format(ArrayEntryFormat, key, ArrayLengthProperty);
            PlayerPrefs.SetInt(countEntry, value.Length);

            for (int i = 0; i < value.Length; ++i)
            {
                PlayerPrefs.SetInt(string.Format(ArrayEntryFormat, key, i), value[i]);
            }
        }

        public static float[] GetFloatArray(string key)
        {
            float[] result = null;

            string countEntry = string.Format(ArrayEntryFormat, key, ArrayLengthProperty);
            if (PlayerPrefs.HasKey(countEntry))
            {
                int size = PlayerPrefs.GetInt(countEntry);

                result = new float[size];

                for (int i = 0; i < size; ++i)
                {
                    string keyEntry = string.Format(ArrayEntryFormat, key, i);
                    if (PlayerPrefs.HasKey(keyEntry))
                    {
                        result[i] = PlayerPrefs.GetFloat(keyEntry);
                    }
                    else
                    {
                        result = null;
                        break;
                    }
                }
            }

            return result;
        }

        public static void SetFloatArray(string key, float[] value)
        {
            string countEntry = string.Format(ArrayEntryFormat, key, ArrayLengthProperty);
            PlayerPrefs.SetInt(countEntry, value.Length);

            for (int i = 0; i < value.Length; ++i)
            {
                PlayerPrefs.SetFloat(string.Format(ArrayEntryFormat, key, i), value[i]);
            }
        }

        public static string[] GetStringArray(string key)
        {
            string[] result = null;

            string countEntry = string.Format(ArrayEntryFormat, key, ArrayLengthProperty);
            if(PlayerPrefs.HasKey(countEntry))
            {
                int size = PlayerPrefs.GetInt(countEntry);

                result = new string[size];

                for (int i = 0; i < size; ++i)
                {
                    string keyEntry = string.Format(ArrayEntryFormat, key, i);
                    if(PlayerPrefs.HasKey(keyEntry))
                    {
                        result[i] = PlayerPrefs.GetString(keyEntry);
                    }
                    else
                    {
                        result = null;
                        break;
                    }
                }
            }

            return result;
        }

        public static void SetStringArray(string key, string[] value)
        {
            string countEntry = string.Format(ArrayEntryFormat, key, ArrayLengthProperty);
            PlayerPrefs.SetInt(countEntry, value.Length);

            for (int i = 0; i < value.Length; ++i)
            {
                PlayerPrefs.SetString(string.Format(ArrayEntryFormat, key, i), value[i]);
            }
        }

        public static void DeleteKey(string key)
        {
            if(HasArray(key))
            {
                DeleteArray(key);
            }
            else
            {
                PlayerPrefs.DeleteKey(key);
            }
        }

        public static void DeleteAll()
        {
            PlayerPrefs.DeleteAll();
        }

        public static void Save()
        {
            PlayerPrefs.Save();
        }

        private static bool HasArray(string key)
        {
            string countEntry = string.Format(ArrayEntryFormat, key, ArrayLengthProperty);
            return PlayerPrefs.HasKey(countEntry);
        }

        private static void DeleteArray(string key)
        {
            string countEntry = string.Format(ArrayEntryFormat, key, ArrayLengthProperty);
            int size = PlayerPrefs.GetInt(countEntry);

            for(int i=0; i<size; ++i)
            {
                PlayerPrefs.DeleteKey(string.Format(ArrayEntryFormat, key, i));
            }

            PlayerPrefs.DeleteKey(countEntry);
        }
    }
}