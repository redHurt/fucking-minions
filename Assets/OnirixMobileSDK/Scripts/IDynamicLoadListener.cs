﻿using Onirix.Core.Model;

namespace Onirix.MobileSDK
{
    public interface IDynamicLoadListener
    {
        void OnTargetAssetsStartDownloading(Target target);
        void OnTargetAssetsDownloaded(Target target);
        void OnTargetAssetsStartLoading(Target target);
        void OnTargetAssetsLoaded(Target target);
    }
}