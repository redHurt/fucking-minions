﻿using System;
using System.Runtime.InteropServices;
using UnityEngine;
using UnityEngine.UI;
using Action = System.Action;

namespace Onirix.MobileSDK
{
    public class LegacyARInterface : ARInterface
    {
        private RawImage _rawImage;
        private GCHandle _camTextureHandle;
        private Matrix4x4 proj;

        public LegacyARInterface()
        {
            float fx = 640;
            float fy = 640;
            float cx = 320;
            float cy = 240;
            float near = 0.1f;
            float far = 1000;

            proj = new Matrix4x4 ();
            proj [0, 0] = fx / cx;
            proj [0, 1] = 0;
            proj [0, 2] = 0;
            proj [0, 3] = 0;
            proj [1, 0] = 0;
            proj [1, 1] = fy / cy;
            proj [1, 2] = 0;
            proj [1, 3] = 0;
            proj [2, 0] = 0;
            proj [2, 1] = 0;
            proj[2, 2] = -(far + near) / (far - near);
            proj [2, 3] = (-2.0f * far * near) / (far - near);
            proj [3, 0] = 0;
            proj [3, 1] = 0;
            proj [3, 2] = -1;
            proj [3, 3] = 0;
            
            Camera.main.projectionMatrix = proj;
        }

        public override void Start()
        {
            WebCamDevice[] devices = WebCamTexture.devices;
            var webcamTexture = new WebCamTexture(devices[0].name, 640, 480, 30);

            webcamTexture.filterMode = FilterMode.Point;

            webcamTexture.Play();
            _rawImage.texture = webcamTexture;
        }

        public void SetRawImageTarget(RawImage rawImage)
        {
            _rawImage = rawImage;
        }

        public override void SetCrosshairManager(OnirixCrosshairManager crosshairManager)
        {
            throw new InvalidOperationException();
        }

        public override ImagePtr GetCameraImage()
        {
            if (_camTextureHandle.IsAllocated)
            {
                _camTextureHandle.Free();
            }
            var pixels = ((WebCamTexture) _rawImage.texture).GetPixels32();
            _camTextureHandle = GCHandle.Alloc(pixels, GCHandleType.Pinned);
            
            return new ImagePtr
            {
                width = _rawImage.texture.width,
                height = _rawImage.texture.height,
                ptr = _camTextureHandle.AddrOfPinnedObject()
            };
        }

        public override bool TryGetPointCloud(ref PointCloud pointCloud)
        {
            throw new InvalidOperationException();
        }

        public override int GetChannelCount()
        {
            return 4;
        }

        public override bool UsesTracking()
        {
            return true;
        }

        public override void ResetScene() {}

        public override bool FlipVert()
        {
            return true;
        }
        
        public override bool SupportsSurface()
        {
            return false;
        }

        public override bool SupportsPointCloud()
        {
            return false;
        }

        public override float GetCameraAspectRatio()
        {
            return 16 / 9.0f;
        }

        public override PlaneInfo? HitPointInScreen(float x, float y)
        {
            throw new InvalidOperationException();
        }

        public override void StartSurface(Action onPlaneDetected, Action onPlaneLost)
        {
            throw new InvalidOperationException();
        }

        public override void StopSurfaceDetection()
        {
            throw new System.InvalidOperationException();
        }

        public override void PlaceTargetOnCrosshair(GameObject gameObject)
        {
            throw new System.InvalidOperationException();
        }

        public override void ReplaceTarget(GameObject oldTarget, GameObject newTarget)
        {
            throw new System.InvalidOperationException();
        }

        public override void Update() {}

        public override void OnNewPose(Matrix4x4 openCVPose)
        {
            
            if (openCVPose == Matrix4x4.zero) return;

            Vector3 openCVposition = new Vector3(openCVPose.m30, openCVPose.m31, openCVPose.m32);
            
            Vector3 scale;
            scale.x = new Vector4 (openCVPose.m00, openCVPose.m01, openCVPose.m02, openCVPose.m03).magnitude;
            scale.y = new Vector4 (openCVPose.m10, openCVPose.m11, openCVPose.m12, openCVPose.m13).magnitude;
            scale.z = new Vector4 (openCVPose.m20, openCVPose.m21, openCVPose.m22, openCVPose.m23).magnitude;

            Vector3 forward = new Vector3(openCVPose.m02, openCVPose.m12, openCVPose.m22);
            Vector3 upwards = new Vector3(openCVPose.m01, openCVPose.m11, openCVPose.m21);
            Quaternion rotation = Quaternion.LookRotation(forward, upwards);
            Debug.Log("Rotation: " + rotation.eulerAngles);

            Camera.main.transform.rotation = rotation;
            Camera.main.transform.position = openCVposition; 
            Camera.main.transform.localScale = scale;    
        }
    }
}