﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using GoogleARCore;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using Action = System.Action;
using Object = UnityEngine.Object;

namespace Onirix.MobileSDK
{
    public class ARCoreInterface : ARInterface
    {
        #region ARCoreCameraAPI

        public const string ARCoreCameraUtilityAPI = "arcore_camera_utility";

        //Texture size. Larger values are slower.
        private const int k_ARCoreTextureWidth = 853;
        private const int k_ARCoreTextureHeight = 480;

        //If imageFormatType is set to ImageFormatColor, the buffer is converted to YUV2.
        //If imageFormatType is set to ImageFormatGrayscale, the buffer is set to Y as is, 
        //while the UV components remain null. Will appear pink using the remote
        private const ImageFormatType k_ImageFormatType = ImageFormatType.ImageFormatColor;

        [DllImport(ARCoreCameraUtilityAPI)]
        public static extern void TextureReader_create(int format, int width, int height, bool keepAspectRatio);

        [DllImport(ARCoreCameraUtilityAPI)]
        public static extern void TextureReader_destroy();

        [DllImport(ARCoreCameraUtilityAPI)]
        public static extern IntPtr TextureReader_submitAndAcquire(
            int textureId, int textureWidth, int textureHeight, ref int bufferSize);

        private enum ImageFormatType
        {
            ImageFormatColor = 0,
            ImageFormatGrayscale = 1
        }

        private byte[] pixelBuffer;

        #endregion

        //private Image _image;

        private List<GameObject>    _placedGameObjects = new List<GameObject>();
        private List<Vector4>       _tempPointCloud = new List<Vector4>();
        private bool                _hasDetectedPlane = false;
        private Action              _onPlaneDetected;
        private Action              _onPlaneLost;
        private Action              _onStopSurface;

        private OnirixCrosshairManager _crosshairManager;


        public override void Start()
        {
            if (Session.Status.IsError())
            {
                switch (Session.Status)
                {
                    case SessionStatus.ErrorApkNotAvailable:
                        Debug.LogError("ARCore APK is not installed");
                        return;
                    case SessionStatus.ErrorPermissionNotGranted:
                        Debug.LogError("A needed permission (likely the camera) has not been granted");
                        return;
                    case SessionStatus.ErrorSessionConfigurationNotSupported:
                        Debug.LogError("The given ARCore session configuration is not supported on this device");
                        return;
                    case SessionStatus.FatalError:
                        Debug.LogError("A fatal error was encountered trying to start the ARCore session");
                        return;
                }
            }
            
            while (!Session.Status.IsValid())
            {
                if (Session.Status.IsError())
                {
                    switch (Session.Status)
                    {
                        case SessionStatus.ErrorPermissionNotGranted:
                            Debug.LogError("A needed permission (likely the camera) has not been granted");
                            return;
                        case SessionStatus.FatalError:
                            Debug.LogError("A fatal error was encountered trying to start the ARCore session");
                            return;
                    }
                }
            }

            TextureReader_create((int) k_ImageFormatType, k_ARCoreTextureWidth, k_ARCoreTextureHeight, true);
        }

        public override void SetCrosshairManager(OnirixCrosshairManager crosshairManager)
        {
            _crosshairManager = crosshairManager;
        }


        public override float GetCameraAspectRatio()
        {
            if (Screen.orientation == ScreenOrientation.Landscape) return 16.0f / 9;
            else return 9 / 16.0f;
        }

        public override ImagePtr GetCameraImage()
        {
            if (Session.Status != SessionStatus.Tracking)
            {
                return new ImagePtr();
            }

            if (Frame.CameraImage.Texture == null || Frame.CameraImage.Texture.GetNativeTexturePtr() == IntPtr.Zero)
            {
                return new ImagePtr();
            }

            //This is a GL texture ID
            int textureId = Frame.CameraImage.Texture.GetNativeTexturePtr().ToInt32();
            int bufferSize = 0;
            //Ask the native plugin to start reading the image of the current frame, 
            //and return the image read from the privous frame
            IntPtr bufferPtr = TextureReader_submitAndAcquire(textureId, k_ARCoreTextureWidth, k_ARCoreTextureHeight, ref bufferSize);

            //I think this is needed because of this bug
            //https://github.com/google-ar/arcore-unity-sdk/issues/66
            GL.InvalidateState();

            if (bufferPtr == IntPtr.Zero || bufferSize == 0)
            {
                return new ImagePtr();
            }

            return new ImagePtr
            {
                ptr = bufferPtr,
                width =  k_ARCoreTextureWidth,
                height = k_ARCoreTextureHeight
            };
            
        }

        public override bool TryGetPointCloud(ref PointCloud pointCloud)
        {
            if (Session.Status != SessionStatus.Tracking)
                return false;

            // Fill in the data to draw the point cloud.
            _tempPointCloud.Clear();
            for (int i = 0; i < Frame.PointCloud.PointCount; i++)
            {
                var point = Frame.PointCloud.GetPointAsStruct(i);
                _tempPointCloud.Add(new Vector4(point.Position.x, point.Position.y, point.Position.z, point.Confidence));
            }

            if (_tempPointCloud.Count > 0)
            {
                if (pointCloud.points == null)
                {
                    pointCloud.points = new List<Vector3>();
                }
                else
                {
                    pointCloud.points.Clear();
                }

                foreach (Vector4 point in _tempPointCloud)
                {
                    pointCloud.points.Add(point);
                }

                return true;
            }

            return false;
        }

        /*
        public override Matrix4x4 GetPose()
        {
            throw new System.NotImplementedException();
        }
        */

        public override int GetChannelCount()
        {
            return 4;
        }

        public override bool UsesTracking()
        {
            return false;
        }

        public override bool FlipVert()
        {
            return false;
        }

        public override bool SupportsSurface()
        {
            return true;
        }

        public override bool SupportsPointCloud()
        {
            return true;
        }

        public override void ResetScene()
        {
            foreach (var obj in _placedGameObjects)
            {
                Object.Destroy(obj);
            }
        }

        public void StartSurface(Action onPlaneLost)
        {
            StartSurface(null, onPlaneLost);
        }

        public override void StartSurface(Action onPlaneDetected, Action onPlaneLost)
        {
            _hasDetectedPlane = false;
            _onPlaneDetected = onPlaneDetected;
            _onPlaneLost = onPlaneLost;
            _onStopSurface = () =>
            {
                _onPlaneDetected = null;
                _onPlaneLost = null;
            };
        }

        public override PlaneInfo? HitPointInScreen(float x, float y)
        {
            TrackableHit hit;
            TrackableHitFlags raycastFilter = TrackableHitFlags.PlaneWithinPolygon |
                                              TrackableHitFlags.PlaneWithinBounds;

            if (Frame.Raycast(x, y, raycastFilter, out hit))
            {       
                PlaneInfo planeInfo = GetPlaneInfoFrom(hit);
                return planeInfo;
            }
            else
            {
                return null;
            }
        }
        
        public override void StopSurfaceDetection()
        {
            _onStopSurface();
        }

        private void updateSurfaceDetection()
        {            
            HitCenterOfScreen(planeInfo =>
            {
                if (!_hasDetectedPlane)
                {
                    _hasDetectedPlane = true;

                    if(_onPlaneDetected != null)
                    {
                        _onPlaneDetected();
                    }
                }

                if(_crosshairManager != null)
                {
                    _crosshairManager.OnCrosshairUpdate(planeInfo);
                }
            }, () =>
            {
                if (_hasDetectedPlane)
                {
                    _hasDetectedPlane = false;

                    if (_onPlaneLost != null)
                    {
                        _onPlaneLost();
                    }

                    if (_crosshairManager != null)
                    {
                        _crosshairManager.OnCrosshairUpdate(null);
                    }
                }
            });
        }

        public override void PlaceTargetOnCrosshair(GameObject gameObject)
        {
            gameObject.transform.position = _crosshairManager.GetCrosshairPosition();
            Quaternion crosshairRot = _crosshairManager.GetCrosshairRotation();
            Quaternion rot = new Quaternion();

            rot.SetLookRotation(crosshairRot * Vector3.up, crosshairRot * Vector3.forward);
            gameObject.transform.rotation = rot;

            _placedGameObjects.Add(gameObject);
        }

        public override void ReplaceTarget(GameObject oldTarget, GameObject newTarget)
        {
            int index = _placedGameObjects.IndexOf(oldTarget);

            if(index>=0)
            {
                newTarget.transform.position = oldTarget.transform.position;
                newTarget.transform.rotation = oldTarget.transform.rotation;

                _placedGameObjects.RemoveAt(index);
                _placedGameObjects.Add(newTarget);
            }
        }

        public override void Update()
        {
            updateSurfaceDetection();
        }

        public override void OnNewPose(Matrix4x4 openCVPose)
        {
        }

        private PlaneInfo GetPlaneInfoFrom(TrackableHit hit)
        {
            PlaneInfo planeInfo = new PlaneInfo();
            DetectedPlane plane = hit.Trackable as DetectedPlane;
            planeInfo.position = hit.Pose.position;

            if (plane != null)
            {
                planeInfo.normal = plane.CenterPose.up;

                if (plane.PlaneType == DetectedPlaneType.Vertical)
                {
                    planeInfo.orientation = Vector3.ProjectOnPlane(Vector3.down, planeInfo.normal);
                }
                else
                {
                    Vector3 orientation = Camera.main.transform.position - planeInfo.position;
                    if(Vector3.Angle(orientation, planeInfo.normal) < 15)
                    {
                        orientation = -Camera.main.transform.up;
                    }
                    orientation.Normalize();

                    planeInfo.orientation = Vector3.ProjectOnPlane(orientation, planeInfo.normal).normalized;
                }
            }

            return planeInfo;
        }

        private void HitCenterOfScreen(Action<PlaneInfo> onHit, Action onMiss)
        {
            PlaneInfo? planeInfo = HitPointInScreen(Screen.width / 2f, Screen.height / 2f);
            if (planeInfo != null)
            {       
                onHit(planeInfo.Value);
            }
            else
            {
                onMiss();
            }
        }

        public static AsyncTask<ApkAvailabilityStatus> IsARCoreSupported()
        {
            return Session.CheckApkAvailability();
        }
    }
}