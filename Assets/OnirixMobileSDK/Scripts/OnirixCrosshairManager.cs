﻿using System;
using UnityEngine;

using Object = UnityEngine.Object;

namespace Onirix.MobileSDK
{
    public class OnirixCrosshairManager : IDisposable
    {
        private readonly ARInterface _arInterface;

        private GameObject      _crosshairInstance;
        private Material        _crosshairFoundMaterial;
        private Material        _crosshairLostMaterial;
        private bool            _isCrosshairHidden;
        private MeshRenderer    _meshRenderer;
        
        private enum Status
        {
            Plane,
            Lost,
            Detecting
        }

        private Status _status = Status.Lost;

        public OnirixCrosshairManager(ARInterface arInterface, GameObject crosshairPrefab, 
            Material crosshairFoundMaterial, Material crosshairLostMaterial)
        {
            _arInterface = arInterface;
            _arInterface.SetCrosshairManager(this);
            _crosshairInstance = Object.Instantiate(crosshairPrefab);
            _crosshairFoundMaterial = crosshairFoundMaterial;
            _crosshairLostMaterial = crosshairLostMaterial;
            _isCrosshairHidden = false;
            _meshRenderer = _crosshairInstance.GetComponentInChildren<MeshRenderer>();
            UpdateCrosshairMaterial(crosshairLostMaterial);
        }

        public void Update()
        {
            if (_status == Status.Lost)
            {
                SetCrosshairInCenter();
            }
        }

        public void OnDetection(PlaneInfo planeInfo, Quaternion openCVRotation)
        {
            _crosshairInstance.transform.position = planeInfo.position;

            Quaternion planeRot = new Quaternion();
            planeRot.SetLookRotation(planeInfo.normal, planeInfo.orientation );

            Vector3 eulerAngles = openCVRotation.eulerAngles;
            eulerAngles.z -= Camera.main.transform.rotation.eulerAngles.z;
            eulerAngles.x = 0;
            eulerAngles.y = 0;

            Quaternion rotZ = new Quaternion();
            rotZ.eulerAngles = eulerAngles;

            _crosshairInstance.transform.rotation = planeRot * rotZ;

            _status = Status.Detecting;
            UpdateCrosshairMaterial(_crosshairFoundMaterial);
        }
        
        public void Show()
        {
            Reset();
        }

        public void Hide()
        {
            _crosshairInstance.SetActive(false);
            _isCrosshairHidden = true;
        }
        
        public void Reset()
        {
            _status = Status.Lost;
            UpdateCrosshairMaterial(_crosshairLostMaterial);
            _crosshairInstance.SetActive(true);
            _isCrosshairHidden = false;
        }

        public bool IsCrosshairLost()
        {
            return _status == Status.Lost;
        }


        public Vector3 GetCrosshairPosition()
        {
            return _crosshairInstance.gameObject.transform.position;
        }

        public Quaternion GetCrosshairRotation()
        {
            return _crosshairInstance.gameObject.transform.rotation;
        }

        public void OnCrosshairUpdate(PlaneInfo? planeInfo)
        {
            if (_status == Status.Lost || _status == Status.Plane)
            {
                if (planeInfo != null)
                {
                    _status = Status.Plane;
                    UpdateCrosshairMaterial(_crosshairFoundMaterial);

                    _crosshairInstance.transform.position = planeInfo.Value.position;
                    Quaternion rot = new Quaternion();
                    rot.SetLookRotation(planeInfo.Value.normal, planeInfo.Value.orientation);
                    _crosshairInstance.transform.rotation = rot;
                }
                else
                {
                    _status = Status.Lost;
                    UpdateCrosshairMaterial(_crosshairLostMaterial);
                    SetCrosshairInCenter();
                }
            }
        }

        private void SetCrosshairInCenter()
        {
            _crosshairInstance.transform.position = Camera.main.transform.position +
                                                    Camera.main.transform.forward.normalized;
            _crosshairInstance.transform.rotation =
                Quaternion.LookRotation(-Camera.main.transform.forward, Vector3.up);
        }

        private void UpdateCrosshairMaterial(Material newMaterial)
        {
            _meshRenderer.sharedMaterial = newMaterial;
        }

        public void ToggleCrosshair()
        {
            if (IsCrosshairHidden())
            {
                Show();
            }
            else
            {
                Hide();
            }
        }

        public bool IsCrosshairHidden()
        {
            return _isCrosshairHidden;
        }

        public void Dispose()
        {
            Object.Destroy(_crosshairInstance);
        }
    }
}