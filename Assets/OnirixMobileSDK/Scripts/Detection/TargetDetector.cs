﻿using System;
using System.Runtime.InteropServices;
using AOT;
using UnityEngine;
using Onirix.Core.Util;

namespace Onirix.MobileSDK.Detection
{
    public class TargetDetector : SingletonBase<TargetDetector>
    {
        #if UNITY_EDITOR
            private const string DllImport = "onirix_targets";
        #elif UNITY_ANDROID && !UNITY_EDITOR
            private const string DllImport = "onirix_targets";
        #elif UNITY_IOS && !UNITY_EDITOR
            private const string DllImport = "__Internal";
        #endif
        
        private bool _detected;
        private bool _tracking;
        private bool _shouldCallOnDetection;
        private string _vocabularyPath;
        private string _otfPath;
        private Action<string, Matrix4x4> _onDetection;
        private string _target;
        private Matrix4x4 _K;

        public delegate void OnTargetDetected(string target);

        public TargetDetector()
        {
        }

        public void ResetDetectionCallback()
        {
            _detected = false;
            _target = null;
            _shouldCallOnDetection = false;
            TargetDetector_resetDetectionCallback();
        }

        public void Reset()
        {
            _detected = false;
            _target = null;
            _shouldCallOnDetection = false;
            this.Destroy();
            this.Init(this._vocabularyPath, this._otfPath, this._tracking, this._onDetection);
        }


        [DllImport(DllImport)]
        private static extern IntPtr TargetDetector_processFrame(IntPtr frame, int width, int height, int channels, int rotation, bool flipVert);
        
        [DllImport(DllImport)]
        private static extern void TargetDetector_resetDetectionCallback();


        [DllImport(DllImport)]
        private static extern void TargetDetector_init(string vocabularyPath, string otfPath,
            bool tracking, OnTargetDetected onTargetDetected, int minGeometryMatches=20);
        
        [DllImport(DllImport)]
        private static extern void TargetDetector_destroy();

        [DllImport(DllImport)]
        private static extern void TargetDetector_stopDetection();


        public Matrix4x4 ProcessFrame(IntPtr frame, int width, int height, int channels, int rotation, bool flipVert)
        {
            //Debug.Log("Sending frame to native...");
            Matrix4x4 pose;
            
            var ptr = TargetDetector_processFrame(frame, width, height, channels, rotation, flipVert);
            if (ptr.ToInt32() == 0)
            {
                pose =  Matrix4x4.zero;
            }
            else
            {
                double[] initialPose = new double[16];
                Marshal.Copy(ptr, initialPose, 0, 16);
                pose = computeOpenCVPose(initialPose);
            }
            
            if (_shouldCallOnDetection && pose != Matrix4x4.zero)
            {
                Debug.Log("Open cv pose: " + pose);
                _detected = true;
                _shouldCallOnDetection = false;
                _onDetection(_target, pose);
            }
    
            return pose;
        }


        public void Init(string vocabularyPath, string otfPath, bool tracking, Action<string, Matrix4x4> onDetection)
        {
            _onDetection = onDetection;
            _vocabularyPath = vocabularyPath;
            _otfPath = otfPath;
            _tracking = tracking;

            int numGeometryMatches = 30;
            TargetDetector_init(vocabularyPath, otfPath, tracking, OnTargetDetectedCalled, numGeometryMatches);
            
            _K = new Matrix4x4();

            _K.m00 = 640;
            _K.m02 = 320;
            _K.m11 = 640;
            _K.m12 = 240;
            _K.m22 = 1;
            
            _shouldCallOnDetection = false;
            _detected = false;
        }

        public void Destroy()
        {
            TargetDetector_destroy();
        }

        public void StopDetection()
        {
            TargetDetector_stopDetection();
        }

        public Matrix4x4 GetK()
        {
            return _K;
        }


        [MonoPInvokeCallback(typeof(OnTargetDetected))]
        private static void OnTargetDetectedCalled(string target)
        {
            if (!Instance._detected)
            {
                Instance._target = target;
                Instance._shouldCallOnDetection = true;
            }
        }

        private Matrix4x4 computeOpenCVPose(double[] pose)
        {
            Matrix4x4 openCVPose = new Matrix4x4();

            openCVPose.m00 = (float)pose[0];
            openCVPose.m01 = (float)pose[1];
            openCVPose.m02 = (float)pose[2];
            openCVPose.m03 = (float)pose[3]; // 0
            openCVPose.m10 = (float)pose[4];
            openCVPose.m11 = (float)pose[5];
            openCVPose.m12 = (float)pose[6];
            openCVPose.m13 = (float)pose[7];// 0
            openCVPose.m20 = (float)pose[8];
            openCVPose.m21 = (float)pose[9];
            openCVPose.m22 = (float)pose[10];
            openCVPose.m23 = (float)pose[11]; // 0
            openCVPose.m30 = (float)pose[12];
            openCVPose.m31 = (float)pose[13];
            openCVPose.m32 = (float)pose[14];
            openCVPose.m33 = (float)pose[15]; // 1

            // The given openCVPose is actually a OpenGL transformation matrix, where the translation
            // elements are in the m03, m13 and m23, so we have to transpose the matrix.

            openCVPose = openCVPose.transpose;
           
            Vector3 normal = new Vector3(openCVPose.m02, openCVPose.m12, openCVPose.m22);
            Vector3 upwards = new Vector3(openCVPose.m01, openCVPose.m11, openCVPose.m21)*-1;
            Vector3 translation = new Vector3(openCVPose.m03, openCVPose.m13, -openCVPose.m23);
            Quaternion rotation = Quaternion.LookRotation(normal.normalized, upwards.normalized).normalized;

            Matrix4x4 result = Matrix4x4.TRS(translation, rotation, Vector3.one);

            /*
            Debug.Log(string.Format("{0} {1} {2} {3}\n{4} {5} {6} {7}\n{8} {9} {10} {11}\n{12} {13} {14} {15}",
                    openCVPose.m00, openCVPose.m01, openCVPose.m02, openCVPose.m03,
                    openCVPose.m10, openCVPose.m11, openCVPose.m12, openCVPose.m13,
                    openCVPose.m20, openCVPose.m21, openCVPose.m22, openCVPose.m23,
                    openCVPose.m30, openCVPose.m31, openCVPose.m32, openCVPose.m33));
            */

            return result;
        }
    }
}