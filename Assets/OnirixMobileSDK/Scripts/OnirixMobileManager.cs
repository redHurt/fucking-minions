using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using GoogleARCore;
using Onirix.MobileSDK.Detection;
using Onirix.Core;
using Onirix.Core.Model;
using Onirix.Core.Service;
using Onirix.Core.Util;
using Onirix.SDK;
using UnityEngine;
using UnityEngine.UI;
using Action = System.Action;

namespace Onirix.MobileSDK
{
    public class OnirixMobileManager : SingletonBehaviour<OnirixMobileManager>
    {
        public enum Status
        {
            Stopped,
            BootingUp,
            Running
        }

        [SerializeField] private bool       _autoBoot = true;
        [SerializeField] private GameObject _legacyCamera;
        [SerializeField] private GameObject _arcoreCamera;
        [SerializeField] private GameObject _arkitCamera;

        #region Crosshair

        [SerializeField] private GameObject _crosshairPrefab;
        [SerializeField] private Material   _crosshairFoundMaterial;
        [SerializeField] private Material   _crosshairLostMaterial;

        private OnirixCrosshairManager      _crosshairManager;

        public Action OnCrosshairHidden { get; set; }
        public Action OnCrosshairShown { get; set; }

        #endregion

        protected ARInterface _arInterface;
        private GameObject _arPrefabInstance;

        private bool _surfaceInnited;
        private bool _detectionInnited;
        private Coroutine _detectionCoroutine;

        private ARCoreSessionConfig _ARCoreSessionConfig;
        private ARCoreSession _ARCoreSession;
        private Status _status = Status.Stopped;


        public bool IsReady
        {
            get
            {
                return _status == Status.Running;
            }
        }

        private void Start()
        {
            if(_autoBoot)
            {
                Boot();
            }
        }


        public void Boot()
        {
            if (_status == Status.Stopped)
            {
                _status = Status.BootingUp;

#if UNITY_EDITOR
                //_webcamTexture.gameObject.SetActive(true);
                _arPrefabInstance = Instantiate(_legacyCamera);
                LegacyARInterface legacyAR = new LegacyARInterface();
                legacyAR.SetRawImageTarget(_arPrefabInstance.GetComponentInChildren<RawImage>());
                _arInterface = legacyAR;
                _arInterface.Start();
                StartCrosshair();
                _status = Status.Running;
#elif UNITY_ANDROID && !UNITY_EDITOR
                ARCoreInterface.IsARCoreSupported().ThenAction(res =>
                {
                    if (res == ApkAvailabilityStatus.SupportedInstalled)
                    {
                        StartARCore();
                        StartCoroutine(StartARCoreInterface());
                    }
                    else
                    {
                        _arPrefabInstance = Instantiate(_legacyCamera);
                        LegacyARInterface legacyAR = new LegacyARInterface();
                        legacyAR.SetRawImageTarget(_arPrefabInstance.GetComponentInChildren<RawImage>());
                        _arInterface = legacyAR;
                        _arInterface.Start();
                        StartCrosshair();
                        _status = Status.Running;
                    }
                });
#elif UNITY_IOS && !UNITY_EDITOR
                _arInterface = new ARKitInterface();
                _arPrefabInstance = Instantiate(_arkitCamera);
                _arInterface.Start();
                StartCrosshair();
                _status = Status.Running;
#endif
                OnirixEventManager.Instance.Run();
            }
        }

        private void StartCrosshair()
        {
            if (_arInterface != null && _arInterface.GetType() != typeof(LegacyARInterface))
            {
                _crosshairManager = new OnirixCrosshairManager(_arInterface, _crosshairPrefab, _crosshairFoundMaterial, _crosshairLostMaterial);
            }
        }

        /// <summary>
        /// Start the marker detection system for a specific project of type marker.
        /// </summary>
        /// <param name="project">Marker project to be started</param>
        /// <param name="key">Project key used to obtain the Onirix Targets File.</param>
        /// <param name="onMarkerDetected">Action to be called when the marker is detected</param>
        /// <param name="otfPath">Optional custom otf (Onirix Targets File) path that will be used to detect the marker.
        /// If no path is specified, the default otf file for the project will be loaded from the Onirix API.</param>
        public bool StartMarkerDetection(Action<string> onMarkerDetected, string forcedOtfPath = "")
        {
            if (IsReady)
            {
#if UNITY_ANDROID && !UNITY_EDITOR
                string vocabularyPath = Path.Combine(Application.persistentDataPath, "orb.fbow");

                if (!File.Exists(vocabularyPath))
                {
                    WWW wwwPath = new WWW(Path.Combine(Application.streamingAssetsPath, "orb.fbow"));
                    while (!wwwPath.isDone) ;
                    File.WriteAllBytes(vocabularyPath, wwwPath.bytes);
                }
#else
                string vocabularyPath = Path.Combine(Application.streamingAssetsPath, "orb.fbow");
#endif

                _detectionCoroutine = StartCoroutine(ImageLoop());

                if (string.IsNullOrEmpty(forcedOtfPath))
                {
                    OnirixRestService.Instance.GetProjectOtf
                    (
                        (otfPath) =>
                        {
                            Debug.Log($"vocabulary: {vocabularyPath}, otfPath: {otfPath}");
                            InitTargetDetector(vocabularyPath, otfPath, _arInterface.UsesTracking(), onMarkerDetected);
                            _detectionInnited = true;
                        }
                    );
                }
                else
                {
                    InitTargetDetector(vocabularyPath, forcedOtfPath, _arInterface.UsesTracking(), onMarkerDetected);
                    _detectionInnited = true;
                }

                return true;
            }

            return false;
        }

        public bool TryGetPointCloud(ref PointCloud pointCloud)
        {
            if(IsReady && _arInterface.SupportsPointCloud())
            {
                return _arInterface.TryGetPointCloud(ref pointCloud);
            }

            return false;
        }

        /// <summary>
        /// Starts the surface detection system.
        /// </summary>
        /// <param name="onSurfaceDetected">Action that will be called when a new surface is detected.</param>
        /// <param name="onSurfaceLost">Action that will be called when a detected surface is lost.</param>
        public bool StartSurfaceDetection(Action onSurfaceDetected, Action onSurfaceLost)
        {
            if (IsReady)
            {
                if (_arInterface.SupportsSurface())
                {
                    _arInterface.StartSurface(
                        () =>
                        {
                            onSurfaceDetected();
                        },
                        () =>
                        {
                            onSurfaceLost();
                        });
                    _surfaceInnited = true;
                }

                return true;
            }

            return false;
        }

        private void InitTargetDetector(string vocabularyPath, string otfPath, bool usesTracking, Action<string> onTargetDetected)
        {
            TargetDetector.Instance.Init(vocabularyPath, otfPath, usesTracking, (oid, pose) =>
            {
            // project point

            Matrix4x4 k = TargetDetector.Instance.GetK();
                Matrix4x4 mult = k * pose;
                Vector4 point = new Vector4(0, 0, 0, 1);
                Vector4 point2 = mult * point;
                Vector4 point3 = point2 / point2.z;

                if (point3.x < 0 || point.y < 0)
                {
                    TargetDetector.Instance.ResetDetectionCallback();
                    return;
                }

            // point in 640x480 to our screen size
            float width = Screen.width;
                float height = Screen.height;
                float ratiox = width / 640.0f;
                float ratioy = height / 480.0f;
                Vector2 point4 = new Vector2(point3.x * ratiox, point3.y * ratioy);

            // crop fix
            float aspectRatio = _arInterface.GetCameraAspectRatio();
                if (aspectRatio > 1.4)
                {
                    float normalizedx = (point3.x - 320) / 640 * 2;
                    float offset = normalizedx * ((width - height) / 1.33f) / 2.0f;
                    point4.x = point4.x - offset;
                }
                else if (aspectRatio < 1.3)
                {
                    float normalizedy = (point3.y - 240) / 480 * 2;
                    float offset = normalizedy * ((height - width) / 1.33f) / 2.0f;
                    point4.y = point4.y - offset;
                }

            // get plane located in screen point
            PlaneInfo? plane = _arInterface.HitPointInScreen(point4.x, point4.y);
                if (plane == null)
                {
                    TargetDetector.Instance.ResetDetectionCallback();
                }
                else
                {
                // move crosshair to point
                _crosshairManager.OnDetection(plane.Value, pose.rotation);

                    onTargetDetected(oid);
                }
            });
        }

        /// <summary>
        /// Resets the current scene, removing all the elements and reseting the crosshair and marker detection system.
        /// </summary>
        public bool ResetScene()
        {
            if (IsReady)
            {
                if (_detectionInnited) TargetDetector.Instance.Reset();
                _arInterface.ResetScene();
                OnirixSceneLoader.ClearTargets();
                if (_crosshairManager != null)
                {
                    _crosshairManager.Reset();
                    ShowCrosshair();
                }

                return true;
            }

            return false;
        }

        /// <summary>
        /// Stop completely the marker and surface detection systems, removing all the elements in the scene.
        /// </summary>
        public void Stop()
        {
            StopMarkerDetection();
            StopSurfaceDetection();

            if (IsReady)
            {
                _arInterface.ResetScene();
            }
            else
            {
                // Just in case
                StopCoroutine(StartARCoreInterface());
            }
            _arInterface = null;
            Destroy(_arPrefabInstance);

            OnirixSceneLoader.ClearTargets();

            if (_crosshairManager != null)
            {
                //_crosshairManager.Reset();
                _crosshairManager.Dispose();
                _crosshairManager = null;
            }

            OnirixEventManager.Instance.Stop();
            OnirixEventManager.Instance.ClearEvents();

            _status = Status.Stopped;
        }

        private IEnumerator ImageLoop()
        {
            if (!_detectionInnited)
            {
                yield return new WaitUntil(() => _detectionInnited);
            }

            while (true)
            {
                var image = _arInterface.GetCameraImage();

                if (image.width > 16 && image.height > 16)
                {
                    Matrix4x4 pose = TargetDetector.Instance.ProcessFrame(image.ptr, image.width, image.height,
                        _arInterface.GetChannelCount(), (int)_arInterface.GetCameraRotation(), _arInterface.FlipVert());
                    _arInterface.OnNewPose(pose);
                }

                yield return null;
            }
        }

        private void StartARCore()
        {
            if (_ARCoreSessionConfig == null)
            {
                _ARCoreSessionConfig = ScriptableObject.CreateInstance<ARCoreSessionConfig>();
            }

            _ARCoreSessionConfig.EnableLightEstimation = false;
            //m_ARCoreSessionConfig.EnablePlaneFinding = true;
            _ARCoreSessionConfig.PlaneFindingMode = DetectedPlaneFindingMode.HorizontalAndVertical;

            //Do we want to match framerate to the camera?
            _ARCoreSessionConfig.MatchCameraFramerate = true;

            // Create a GameObject on which the session component will live.
            if (_ARCoreSession == null)
            {
                var go = new GameObject("ARCore Session");
                go.SetActive(false);
                _ARCoreSession = go.AddComponent<ARCoreSession>();
                _ARCoreSession.SessionConfig = _ARCoreSessionConfig;
                go.SetActive(true);

                // Enabling the session triggers the connection
                _ARCoreSession.enabled = true;
            }
            else
            {
                _ARCoreSession.SessionConfig = _ARCoreSessionConfig;
            }
        }

        private IEnumerator StartARCoreInterface()
        {
            // wait to init ARCore interface until ARCore session is ready
            while (!(SessionStatusExtensions.IsValid(Session.Status)))
            {
                yield return null;
            }

            _arPrefabInstance = Instantiate(_arcoreCamera);
            _arInterface = new ARCoreInterface();
            _arInterface.Start();
            StartCrosshair();

            _status = Status.Running;
        }

        /// <summary>
        /// Place a specific target in the crosshair.
        /// </summary>
        /// <param name="targetGameObject">Gameobject of the target to be placed.</param>
        public void PlaceTargetOnCrosshair(GameObject targetGameObject)
        {
            _arInterface.PlaceTargetOnCrosshair(targetGameObject);
        }

        public void ReplaceTarget(GameObject oldTarget, GameObject newTarget)
        {
            _arInterface.ReplaceTarget(oldTarget, newTarget);
        }

        private void Update()
        {
            if (_status == Status.Running)
            {
                if (_arInterface != null)
                {
                    _arInterface.Update();
                }

                if (_crosshairManager != null)
                {
                    _crosshairManager.Update();
                }
            }
        }

        public void StopSurfaceDetection()
        {
            if (_surfaceInnited)
            {
                _arInterface.StopSurfaceDetection();
                _surfaceInnited = false;
            }
        }

        public void StopMarkerDetection()
        {
            if (_detectionInnited)
            {
                StopCoroutine(_detectionCoroutine);
                _detectionCoroutine = null;
                TargetDetector.Instance.Destroy();
                _detectionInnited = false;
            }
        }

        public static void FetchDeviceSupportsSurfaces(Action<bool> supportsSurfaces)
        {
#if UNITY_EDITOR
            supportsSurfaces(false);
#elif UNITY_ANDROID && !UNITY_EDITOR
            ARCoreInterface.IsARCoreSupported().ThenAction((result) => { supportsSurfaces(result == ApkAvailabilityStatus.SupportedInstalled); });
#elif UNITY_IOS && !UNITY_EDITOR
            supportsSurfaces(ARKitInterface.IsARKitSupported());
#endif
        }

        public Vector3 GetCrosshairPosition()
        {
            if (_crosshairManager != null)
            {
                return _crosshairManager.GetCrosshairPosition();
            }

            return Vector3.zero;
        }

        public Quaternion GetCrosshairRotation()
        {
            if (_crosshairManager != null)
            {
                return _crosshairManager.GetCrosshairRotation();
            }

            return Quaternion.identity;
        }

        public bool IsCrosshairHidden()
        {
            if (_crosshairManager != null)
            {
                return _crosshairManager.IsCrosshairHidden();
            }

            return true;
        }

        public void ToggleCrosshair()
        {
            if (_crosshairManager != null && _crosshairManager.IsCrosshairHidden())
            {
                ShowCrosshair();
            }
            else
            {
                HideCrosshair();
            }
        }

        public void ResetCrosshair()
        {
            _crosshairManager?.Reset();
        }

        /// <summary>
        /// Hide the crosshair from the screen.
        /// </summary>
        public void HideCrosshair()
        {
            if (_crosshairManager != null)
            {
                _crosshairManager.Hide();

                OnCrosshairHidden?.Invoke();
            }
        }


        /// <summary>
        /// Show the crosshair on screen.
        /// </summary>
        public void ShowCrosshair()
        {
            if (_crosshairManager != null)
            {
                _crosshairManager.Show();

                OnCrosshairShown?.Invoke();
            }
        }
    }
}