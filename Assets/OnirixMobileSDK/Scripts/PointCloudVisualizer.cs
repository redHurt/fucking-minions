﻿using UnityEngine;

namespace Onirix.MobileSDK
{
    public class PointCloudVisualizer : MonoBehaviour
    {
        [SerializeField]
        private ParticleSystem              _pointCloudParticlePrefab;

        [SerializeField]
        private int                         _maxPointsToShow = 300;

        [SerializeField]
        private float                       _particleSize = 0.01f;

        private ParticleSystem              _particleSystem;
        private ParticleSystem.Particle[]   _particles;
        private ParticleSystem.Particle[]   _noParticles;
        private PointCloud                  _pointCloud;
        private OnirixMobileManager         _mobileManager;

        private void OnDisable()
        {
            _particleSystem?.SetParticles(_noParticles, 1);
        }

        // Use this for initialization
        void Start()
        {
            _particleSystem = Instantiate(_pointCloudParticlePrefab);
            _noParticles = new ParticleSystem.Particle[1];
            _noParticles[0].startSize = 0f;
        }

        private void OnDestroy()
        {
            if (_particleSystem)
            {
                Destroy(_particleSystem.gameObject);
                _particleSystem = null;
            }
        }

        // Update is called once per frame
        void Update()
        {
            OnirixMobileManager mobileManager = OnirixMobileManager.Instance;


            if (_particleSystem && mobileManager != null && mobileManager.TryGetPointCloud(ref _pointCloud))
            {
                int numParticles = Mathf.Min(_pointCloud.points.Count, _maxPointsToShow);

                if (_particles == null || _particles.Length != numParticles)
                {
                    _particles = new ParticleSystem.Particle[numParticles];
                }

                for (int i = 0; i < numParticles; ++i)
                {
                    _particles[i].position = _pointCloud.points[i];
                    _particles[i].startColor = new Color(1.0f, 1.0f, 1.0f);
                    _particles[i].startSize = _particleSize;
                }

                _particleSystem.SetParticles(_particles, numParticles);
            }
            else
            {
                _particleSystem.SetParticles(_noParticles, 1);
            }
        }
    }
}
