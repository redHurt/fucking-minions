﻿using System;
using System.Collections;
using UnityEngine;

namespace Onirix.MobileSDK.Prefabs
{
    public class OnirixLoadingElement : MonoBehaviour
    {
        readonly Vector3    MinScale            = new Vector3(0.001f, 0.001f, 0.001f);
        readonly Vector3    MaxScale            = new Vector3(0.025f, 0.025f, 0.025f);
        readonly Color      OpaqueColor         = new Color(1, 1, 1, 1);
        readonly Color      TransparentColor    = new Color(1, 1, 1, 0);
        const float         AnimationSeconds    = 3.0f;
        const float         FadeStartSeconds    = 2.0f;
        const float         DelayBetweenPlanes  = 1.5f;

        [SerializeField] private GameObject _plane1;
        [SerializeField] private GameObject _plane2;

        void Start()
        {
            // ******************
            // Animations
            // ******************
            Action<GameObject> scalePlane = null;
            scalePlane = (plane) =>
            {
                StartCoroutine(RunScale(plane, MinScale, MaxScale, AnimationSeconds, () =>
                {
                    scalePlane(plane);
                }));
            };

            Action<GameObject> fadePlane = null;
            fadePlane = (plane) =>
            {
                StartCoroutine(RunFade(plane, OpaqueColor, TransparentColor, FadeStartSeconds, AnimationSeconds-FadeStartSeconds, () =>
                {
                    fadePlane(plane);
                }));
            };

            _plane2.SetActive(false);

            // ******************
            // Plane 1 animations
            // ******************
            scalePlane(_plane1);
            fadePlane(_plane1);

            // ******************
            // Plane 2 animations
            // ******************
            StartCoroutine(Delay(DelayBetweenPlanes, () => {_plane2.SetActive(true); scalePlane(_plane2); }));
            StartCoroutine(Delay(DelayBetweenPlanes, () => fadePlane(_plane2)));
        }

        private void OnDestroy()
        {
            // Destroy custom materials to avoid leak

            Destroy(_plane1.GetComponent<MeshRenderer>().material);
            Destroy(_plane2.GetComponent<MeshRenderer>().material);
        }

        private IEnumerator RunScale(GameObject obj, Vector3 startScale, Vector3 endScale, float time, Action onEnd)
        {
            obj.transform.localScale = startScale;
            obj.SetActive(true);

            float currentTime = 0;
            while(currentTime < time)
            {
                currentTime += Time.deltaTime;
                obj.transform.localScale = startScale + (endScale - startScale) * (currentTime / time);
                yield return null;
            }
            obj.transform.localScale = endScale;
            onEnd();
        }

        private IEnumerator RunFade(GameObject obj, Color initColor, Color endColor, float initialDelay, float time, Action onEnd)
        {
            MeshRenderer meshRenderer = obj.GetComponent<MeshRenderer>();
            meshRenderer.material.color = initColor;
            yield return new WaitForSeconds(initialDelay);

            float currentTime = 0;
            while (currentTime < time)
            {
                currentTime += Time.deltaTime;

                float elapsed_0_1 = (currentTime / time);
                meshRenderer.material.color = new Color
                (
                    initColor.r + (endColor.r - initColor.r) * elapsed_0_1,
                    initColor.g + (endColor.g - initColor.g) * elapsed_0_1,
                    initColor.b + (endColor.b - initColor.b) * elapsed_0_1,
                    initColor.a + (endColor.a - initColor.a) * elapsed_0_1
                );
                yield return null;
            }
            meshRenderer.material.color = endColor;
            onEnd();
        }

        private IEnumerator Delay(float delay, Action onEnd)
        {
            yield return new WaitForSeconds(delay);
            onEnd();
        }
    }
}