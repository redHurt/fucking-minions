﻿namespace Onirix.MobileSDK
{
    public enum RotateFlags
    {
        Rotate90Clockwise = 0,
        Rotate180 = 1,
        Rotate90CounterClockwise = 2,
        None = 3
    }
}