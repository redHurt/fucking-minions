﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using GoogleARCore;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.XR.iOS;
using Action = System.Action;
using ARAnchor = UnityARInterface.ARAnchor;
using Debug = UnityEngine.Debug;
using Object = UnityEngine.Object;

namespace Onirix.MobileSDK
{
    public class ARKitInterface : ARInterface
    {
        private UnityARSessionNativeInterface nativeInterface
        {
            get { return UnityARSessionNativeInterface.GetARSessionNativeInterface(); }
        }

        private int _cameraWidth;
        private int _cameraHeight;
        private byte[] _textureYBytes;
        private byte[] _textureUVBytes;
        private bool _texturesInitialized;

        private UnityARAnchorManager _anchorManager;

        private Matrix4x4 proj;


        private List<GameObject> _placedGameObjects;
        private Action _onPlaneDetected;
        private Action _onPlaneLost;
        private Action _onStopSurface;
        private bool _hasDetectedPlane;

        private GCHandle _pinnedYArray;
        private GCHandle _pinnedUVArray;
        private ARKitWorldTrackingSessionConfiguration _sessionConfig;
        private Vector3[] _pointCloudData;
        private OnirixCrosshairManager _crosshairManager;

        public static bool IsARKitSupported()
        {
            ARKitWorldTrackingSessionConfiguration config = new ARKitWorldTrackingSessionConfiguration(
                UnityARAlignment.UnityARAlignmentGravity,
                UnityARPlaneDetection.HorizontalAndVertical,
                true
            );

            return config.IsSupported;
        }

        public override void Start()
        {
            _anchorManager = new UnityARAnchorManager();

            _hasDetectedPlane = false;
            _placedGameObjects = new List<GameObject>();
            _texturesInitialized = false;
            UnityARSessionNativeInterface.ARFrameUpdatedEvent += UpdateCamera;

            _sessionConfig = new ARKitWorldTrackingSessionConfiguration(
                UnityARAlignment.UnityARAlignmentGravity,
                UnityARPlaneDetection.HorizontalAndVertical,
                true
            );

            if (!_sessionConfig.IsSupported)
            {
                Debug.LogError("The requested ARKit session configuration is not supported");
                return;
            }

            UnityARSessionRunOption runOptions =
                UnityARSessionRunOption.ARSessionRunOptionRemoveExistingAnchors |
                UnityARSessionRunOption.ARSessionRunOptionResetTracking;

            nativeInterface.RunWithConfigAndOptions(_sessionConfig, runOptions);
        }

        public override void SetCrosshairManager(OnirixCrosshairManager crosshairManager)
        {
            _crosshairManager = crosshairManager;
        }

        public override void StartSurface(Action onPlaneDetected, Action onPlaneLost)
        {
            _hasDetectedPlane = false;
            _onPlaneDetected = onPlaneDetected;
            _onPlaneLost = onPlaneLost;
            _onStopSurface = () =>
            {
                _onPlaneDetected = null;
                _onPlaneLost = null;
                _onStopSurface = null;
            };
        }

        public override void StopSurfaceDetection()
        {
            _onStopSurface();
        }

        private PlaneInfo GetPlaneInfoFrom(ARHitTestResult hitResult)
        {
            PlaneInfo planeInfo = new PlaneInfo();

            var matrix = hitResult.worldTransform;

            planeInfo.position = UnityARMatrixOps.GetPosition(matrix);

            Quaternion rot = UnityARMatrixOps.GetRotation(matrix);
            planeInfo.normal = rot* Vector3.up;

            string anchorIdentifier = hitResult.anchorIdentifier;
            var planeAnchor = _anchorManager.GetCurrentPlaneAnchorsDict()[anchorIdentifier].planeAnchor;
            if (planeAnchor.alignment == ARPlaneAnchorAlignment.ARPlaneAnchorAlignmentVertical)
            {
                planeInfo.orientation = Vector3.ProjectOnPlane(Vector3.down, planeInfo.normal);
            }
            else
            {
                Vector3 orientation = Camera.main.transform.position - planeInfo.position;
                if (Vector3.Angle(orientation, planeInfo.normal) < 15)
                {
                    orientation = -Camera.main.transform.up;
                }
                orientation.Normalize();

                planeInfo.orientation = Vector3.ProjectOnPlane(orientation, planeInfo.normal).normalized;
            }

            return planeInfo;
        }

        private void updateSurfaceDetection()
        {
            if (_onPlaneDetected != null && _onPlaneLost != null)
            {
                HitCenterOfScreen(planeInfo =>
                {
                    if (!_hasDetectedPlane)
                    {
                        _hasDetectedPlane = true;
                        _onPlaneDetected.Invoke();
                    }

                    if (_crosshairManager != null)
                    {
                        _crosshairManager.OnCrosshairUpdate(planeInfo);
                    }

                }, () =>
                {
                    if (_hasDetectedPlane)
                    {
                        _hasDetectedPlane = false;
                        _onPlaneLost.Invoke();

                        if (_crosshairManager != null)
                        {
                            _crosshairManager.OnCrosshairUpdate(null);
                        }
                    }
                });
            }
        }

        public override void PlaceTargetOnCrosshair(GameObject gameObject)
        {
            gameObject.transform.position = _crosshairManager.GetCrosshairPosition();
            Quaternion crosshairRot = _crosshairManager.GetCrosshairRotation();
            Quaternion rot = new Quaternion();

            rot.SetLookRotation(crosshairRot * Vector3.up, crosshairRot * Vector3.forward);
            gameObject.transform.rotation = rot;

            _placedGameObjects.Add(gameObject);
        }

        public override void ReplaceTarget(GameObject oldTarget, GameObject newTarget)
        {
            int index = _placedGameObjects.IndexOf(oldTarget);

            if (index >= 0)
            {
                newTarget.transform.position = oldTarget.transform.position;
                newTarget.transform.rotation = oldTarget.transform.rotation;

                _placedGameObjects.RemoveAt(index);
                _placedGameObjects.Add(newTarget);
            }
        }

        public override PlaneInfo? HitPointInScreen(float x, float y)
        {
            Vector3 screenPosition = Camera.main.ScreenToViewportPoint(new Vector2(x, y));

            
            ARPoint point = new ARPoint
            {
                x = screenPosition.x,
                y = screenPosition.y
            };

            var resultType = ARHitTestResultType.ARHitTestResultTypeExistingPlaneUsingGeometry;
            var result = HitTestWithResultType(point, resultType);
            if (result != null)
            {
                return GetPlaneInfoFrom(result.Value);
            }
            else
            {
                return null;
            }
        }

        private ARHitTestResult? HitTestWithResultType(ARPoint point, ARHitTestResultType resultTypes)
        {
            var results = nativeInterface.HitTest(point, resultTypes);
            return results.Count > 0 ? results.First() : (ARHitTestResult?) null;
        }

        void UpdateCamera(UnityARCamera camera)
        {
            if (!_texturesInitialized)
            {
                InitializeTextures(camera);
            }

            _pointCloudData = camera.pointCloudData;

            //UnityARSessionNativeInterface.ARFrameUpdatedEvent -= UpdateCamera;
        }

        void InitializeTextures(UnityARCamera camera)
        {
            _cameraWidth = camera.videoParams.yWidth;
            _cameraHeight = camera.videoParams.yHeight;

            int numYBytes = _cameraWidth * _cameraHeight;
            int numUVBytes = _cameraWidth * _cameraHeight / 2;

            _textureYBytes = new byte[numYBytes];
            _textureUVBytes = new byte[numUVBytes];

            _pinnedYArray = GCHandle.Alloc(_textureYBytes, GCHandleType.Pinned);
            _pinnedUVArray = GCHandle.Alloc(_textureUVBytes, GCHandleType.Pinned);
            _texturesInitialized = true;
        }


        public override ImagePtr GetCameraImage()
        {
            if (!_texturesInitialized)
                return new ImagePtr();

            nativeInterface.SetCapturePixelData(true, _pinnedYArray.AddrOfPinnedObject(),
                _pinnedUVArray.AddrOfPinnedObject());

            return new ImagePtr
            {
                ptr = _pinnedYArray.AddrOfPinnedObject(),
                width = _cameraWidth,
                height = _cameraHeight
            };
        }

        public override bool TryGetPointCloud(ref PointCloud pointCloud)
        {
            if (_pointCloudData == null)
                return false;

            if (pointCloud.points == null)
                pointCloud.points = new List<Vector3>();

            pointCloud.points.Clear();
            pointCloud.points.AddRange(_pointCloudData);

            return true;
        }

        public override float GetCameraAspectRatio()
        {
            if (Screen.orientation == ScreenOrientation.Landscape) return 16.0f / 9;
            else return 9 / 16.0f;
        }

        public override void ResetScene()
        {
            foreach (var obj in _placedGameObjects)
            {
                Object.Destroy(obj);
            }
        }

        /*
        public override Matrix4x4 GetPose()
        {
            throw new System.NotImplementedException();
        }
        */

        public override int GetChannelCount()
        {
            return 1;
        }

        public override bool UsesTracking()
        {
            return false;
        }

        public override bool FlipVert()
        {
            return false;
        }

        public override bool SupportsSurface()
        {
            return true;
        }

        public override bool SupportsPointCloud()
        {
            return true;
        }

        public override void Update()
        {
            Camera.main.transform.localPosition =
                UnityARMatrixOps.GetPosition(nativeInterface.GetCameraPose());
            Camera.main.transform.localRotation =
                UnityARMatrixOps.GetRotation(nativeInterface.GetCameraPose());
            Camera.main.projectionMatrix = nativeInterface.GetCameraProjection();
            
            updateSurfaceDetection();
        }

        public override void OnNewPose(Matrix4x4 openCVPose)
        {
        }

        private void HitCenterOfScreen(Action<PlaneInfo> onHit, Action onMiss)
        {
            var planeInfo = HitPointInScreen(Screen.width / 2f, Screen.height / 2f);
            if (planeInfo != null)
            {
                onHit(planeInfo.Value);
            }
            else
            {
                onMiss();
            }
        }
    }
}