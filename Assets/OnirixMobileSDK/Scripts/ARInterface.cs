﻿using System;
using System.Collections.Generic;
using GoogleARCore;
using UnityEngine;
using UnityEngine.UI;
using Action = System.Action;

namespace Onirix.MobileSDK
{
    public struct ImagePtr
    {
        public IntPtr ptr;
        public int width;
        public int height;
    }

    public struct PointCloud
    {
        public List<Vector3> points;
    }

    public abstract class ARInterface
    {
        public abstract void Start();

        public abstract void SetCrosshairManager(OnirixCrosshairManager crosshairManager);

        public abstract ImagePtr GetCameraImage();

        public abstract bool TryGetPointCloud(ref PointCloud pointCloud);

        public abstract float GetCameraAspectRatio();

        public abstract int GetChannelCount();

        public abstract PlaneInfo? HitPointInScreen(float x, float y);

        public abstract bool UsesTracking();

        public RotateFlags GetCameraRotation()
        {
            switch (Screen.orientation)
            {
                case ScreenOrientation.LandscapeLeft:
                    return RotateFlags.None;
                case ScreenOrientation.LandscapeRight:
                    return RotateFlags.Rotate180;
                case ScreenOrientation.Portrait:
                    return RotateFlags.Rotate90Clockwise;
                case ScreenOrientation.PortraitUpsideDown:
                    return RotateFlags.Rotate90CounterClockwise;
                default:
                    return RotateFlags.None;
            }
        }

        public abstract bool FlipVert();

        public abstract bool SupportsSurface();

        public abstract bool SupportsPointCloud();

        public abstract void StartSurface(Action onPlaneDetected, Action onPlaneLost);

        public abstract void PlaceTargetOnCrosshair(GameObject gameObject);

        public abstract void ReplaceTarget(GameObject oldTarget, GameObject newTarget);

        public abstract void StopSurfaceDetection();

        public abstract void Update();

        public abstract void OnNewPose(Matrix4x4 openCVPose);

        public abstract void ResetScene();
    }
}