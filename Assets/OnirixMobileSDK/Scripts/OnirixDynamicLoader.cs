﻿using System.Collections.Generic;
using Onirix.Core;
using Onirix.Core.Model;
using Onirix.Core.Util;
using Onirix.Core.Service;
using Onirix.SDK.Util.ToastPlugin;
using UnityEngine;
using Space = Onirix.Core.Model.Space;

namespace Onirix.MobileSDK
{
    public class OnirixDynamicLoader : SingletonBehaviour<OnirixDynamicLoader>
    {
        [SerializeField] private GameObject             _loadingTargetPlaceholder;

        private OnirixSceneLoader                       _sceneLoader;

        protected Dictionary<string, List<GameObject>>  Placeholders { get; private set; }
        protected IDynamicLoadListener                  Listener { get; private set; }

        private class SceneLoaderInterface : IOnirixSceneLoaderDelegate
        {
            public void OnLoadError(RestError error)
            {
                ToastHelper.ShowToast(error.Messages[0]);
            }

            public void OnSpaceSceneEvent(Space space, GameObject spaceGameObject, SceneLoaderEvent ev)
            {
                throw new System.NotImplementedException();
            }

            public void OnTargetSceneEvent(Target target, GameObject targetGameObject, SceneLoaderEvent ev)
            {
                if (ev == SceneLoaderEvent.SceneLoaded)
                {                    
                    List<GameObject> targetPlaceholders;
                    if(Instance.Placeholders.TryGetValue(target.Oid, out targetPlaceholders))
                    {
                        if (Instance.Listener != null)
                        {
                            Instance.Listener.OnTargetAssetsLoaded(target);
                        }

                        GameObject placeholder = targetPlaceholders[0];
                        OnirixMobileManager.Instance.ReplaceTarget(placeholder, targetGameObject);
                        targetPlaceholders.Remove(placeholder);
                        Destroy(placeholder);

                        if (targetPlaceholders.Count == 0)
                        {
                            Instance.Placeholders.Remove(target.Oid);
                        }
                        else
                        {
                            foreach (GameObject obj in targetPlaceholders)
                            {
                                Instance._sceneLoader.LoadTargetInScene(target.Oid);
                            }
                        }
                    }
                    else
                    {
                        // Object placeholder was removed from scene
                        Destroy(targetGameObject);
                    }
                }
                else if (ev == SceneLoaderEvent.AssetsRequested && Instance.Listener != null)
                {
                    Instance.Listener.OnTargetAssetsStartDownloading(target);
                }
                else if (ev == SceneLoaderEvent.AssetsDownloaded && Instance.Listener != null)
                {
                    Instance.Listener.OnTargetAssetsDownloaded(target);
                    Instance.Listener.OnTargetAssetsStartLoading(target);
                }
            }
        }

        protected override void Awake()
        {
            base.Awake();

            _sceneLoader = new OnirixSceneLoader();
            _sceneLoader.SetDelegate(new SceneLoaderInterface());
        }

        public void Init(IDynamicLoadListener listener = null)
        {
            Placeholders = new Dictionary<string, List<GameObject>>();
            Listener = listener;
        }

        public void Clear()
        {
            foreach (var pair in Placeholders)
            {
                foreach (GameObject obj in pair.Value)
                {
                    Destroy(obj);
                }
            }
            Placeholders.Clear();
        }

        public void LoadTarget(Target target)
        {
            LoadTarget(target.Oid);
        }

        public void LoadTarget(string oid)
        {
            if (TargetIsLoading(oid))
            {
                // Just add the placeholder
                PutPlaceholderOnCrosshair(oid);
            }
            else
            {
                PutPlaceholderOnCrosshair(oid);
                _sceneLoader.LoadTargetInScene(oid);
            }
        }

        protected bool TargetIsLoading(string oid)
        {
            return Placeholders.ContainsKey(oid);
        }

        protected void PutPlaceholderOnCrosshair(string oid)
        {
            List<GameObject> targetPlaceholders;
            if (!Placeholders.TryGetValue(oid, out targetPlaceholders))
            {
                targetPlaceholders = new List<GameObject>();
                Placeholders.Add(oid, targetPlaceholders);
            }

            // Create object
            GameObject obj = Instantiate(_loadingTargetPlaceholder);
            obj.name = string.Format("{0}_placeholder_{1}", oid, targetPlaceholders.Count);

            GameObject rootObject = GameObject.Find("Targets");
            if (rootObject == null)
            {
                Debug.Log("Parent gameobject for targets doesn't exist. Creating...");
                rootObject = new GameObject("Targets");
            }
            obj.transform.parent = rootObject.transform;

            targetPlaceholders.Add(obj);

            // Put placeholder
            OnirixMobileManager.Instance.PlaceTargetOnCrosshair(obj);
        }
    }
}