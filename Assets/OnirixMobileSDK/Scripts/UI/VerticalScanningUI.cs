﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Onirix.MobileSDK.UI
{
    [RequireComponent(typeof(PointCloudVisualizer))]
    public class VerticalScanningUI : MonoBehaviour
    {
        // Inspector fields

        [SerializeField] private float  _upwardsScanningTime    = 3f;
        [SerializeField] private float  _downwardsScanningTime  = 1.5f;
        [SerializeField] private float  _scanningBarThickness   = 220.0f;
        [SerializeField] private Color  _scanningBarColor       = new Color(255,255,255,94);
              
        private PointCloudVisualizer    _pointCloudVisualizer;
        private GameObject              _bar;
        private RectTransform           _barTransform;
        private Coroutine               _scanningCoroutine;


        // Use this for initialization
        private void Start()
        {
            // Get point cloud visualizer
            _pointCloudVisualizer = GetComponent<PointCloudVisualizer>();
            _pointCloudVisualizer.enabled = false;

            // Create bar
            GameObject bar = new GameObject("ScanningBar");
            Image barImage = bar.AddComponent<Image>();
            barImage.color = _scanningBarColor;
            bar.transform.SetParent(gameObject.transform);
            _bar = bar;

            RectTransform rt = bar.GetComponent<RectTransform>();
            //rt.anchoredPosition = new Vector2(0.5f, 0.0f);
            rt.anchorMin = new Vector2(0.0f, 0.0f);
            rt.anchorMax = new Vector2(1.0f, 0.0f);
            rt.pivot = new Vector2(0.5f, 0.0f);
            rt.sizeDelta = new Vector2(0, _scanningBarThickness);
            rt.anchoredPosition = Vector2.zero;
            rt.localScale = Vector3.one;
            _barTransform = rt;

            // Initially disabled
            _bar.SetActive(false);
            _scanningCoroutine = null;
        }

        public bool Scanning
        {
            get
            {
                return _scanningCoroutine != null;
            }
            set
            {
                if ((_scanningCoroutine != null) != value)
                {
                    _bar.SetActive(value);

                    if(value)
                    {
                        _scanningCoroutine = StartCoroutine(ScanningCoroutine());
                    }
                    else
                    {
                        StopCoroutine(_scanningCoroutine);
                        _scanningCoroutine = null;
                    }

                    // Show point cloud
                    _pointCloudVisualizer.enabled = value;
                }
            }
        }


        private IEnumerator ScanningCoroutine()
        {
            RectTransform parentRectTransform = _bar.transform.parent.GetComponent<RectTransform>();

            while (true)
            {
                // Rest position to zero
                _barTransform.anchoredPosition = new Vector2(_barTransform.anchoredPosition.x, 0);

                float maxY;
                // Scanning upwards
                float currentTime = 0;

                while (currentTime < _upwardsScanningTime)
                {
                    currentTime += Time.deltaTime;
                    float elapsed_0_1 = (currentTime / _upwardsScanningTime);
                    float smoothedElapsed = Mathf.SmoothStep(0.0f, 1.0f, elapsed_0_1);

                    // REMARK: Refresh maxY taking into account the device orientation changes
                    maxY = parentRectTransform.rect.height - _barTransform.rect.height;
                    _barTransform.anchoredPosition = new Vector2(_barTransform.anchoredPosition.x, 
                        maxY * smoothedElapsed);

                    yield return null;
                }

                maxY = parentRectTransform.rect.height - _barTransform.rect.height;
                _barTransform.anchoredPosition = new Vector2(_barTransform.anchoredPosition.x, maxY);

                // Scanning downwards
                currentTime = 0;
                while (currentTime < _downwardsScanningTime)
                {
                    currentTime += Time.deltaTime;
                    float elapsed_0_1 = (currentTime / _downwardsScanningTime);

                    // REMARK: Refresh maxY taking into account the device orientation changes
                    maxY = parentRectTransform.rect.height - _barTransform.rect.height;
                    _barTransform.anchoredPosition = new Vector2(_barTransform.anchoredPosition.x, 
                        maxY - maxY * elapsed_0_1);

                    yield return null;
                }

                _barTransform.anchoredPosition = new Vector2(_barTransform.anchoredPosition.x, 0);
            }
        }
    }
}
