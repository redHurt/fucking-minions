﻿using UnityEngine;

namespace Onirix.MobileSDK
{
    public struct PlaneInfo
    {
        public Vector3 position;
        public Vector3 normal;
        public Vector3 orientation;
    }
}